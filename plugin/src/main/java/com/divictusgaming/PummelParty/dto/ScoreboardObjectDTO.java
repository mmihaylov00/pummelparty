package com.divictusgaming.PummelParty.dto;

import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;

public class ScoreboardObjectDTO {
    private final Scoreboard scoreboard;
    private final HashMap<Team, String> teams;
    private boolean doUpdate;
    //in case we want to update only on change, set this to true for a one time update
    private boolean forceUpdate;


    public ScoreboardObjectDTO(Scoreboard scoreboard, HashMap<Team, String> teams, boolean doUpdate) {
        this.scoreboard = scoreboard;
        this.teams = teams;
        this.doUpdate = doUpdate;
        forceUpdate = false;
    }

    public boolean isDoUpdate() {
        return doUpdate;
    }

    public HashMap<Team, String> getTeams() {
        return teams;
    }

    public Scoreboard getScoreboard() {
        return scoreboard;
    }

    //it will be auto set to false after getting
    public boolean isForceUpdate() {
        if (forceUpdate) {
            forceUpdate = false;
            return true;
        }
        return false;
    }

    public void makeForceUpdate() {
        this.forceUpdate = true;
    }
    public void changeUpdate(boolean update) {
        this.doUpdate = update;
    }
}