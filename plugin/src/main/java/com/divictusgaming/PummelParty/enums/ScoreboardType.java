package com.divictusgaming.PummelParty.enums;

public enum ScoreboardType {
    LOBBY,
    WAITING_LOBBY,
    IN_GAME,
    MINIGAME

}
