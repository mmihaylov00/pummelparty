package com.divictusgaming.PummelParty.enums;

import com.divictusgaming.PummelParty.board.Board;

import java.util.function.Predicate;

public enum BoardGui {
    //todo load values from config => change enum to object
    PLAY_TIME("CLOCK", "&fPlay time: &a{time}", "Editor", 1, null),
    PLAY_TIME_ADD("<glow>ARROW", "&a+ 1 Minute", "Editor:timeup", 0, (board -> {
        board.setPlayTime(board.getPlayTime() + 60);
        return true;
    })),
    PLAY_TIME_REMOVE("ARROW", "&c- 1 Minute", "Editor:timedown", 2, (board -> {
        if (board.getPlayTime() > 60) {
            board.setPlayTime(board.getPlayTime() - 60);
            return true;
        }
        return false;
    })),

    MIN_PLAYERS("PLAYER_HEAD_ITEM", "&fMin players: &a{minPlayers}", "Editor", 1, null),
    MIN_PLAYERS_ADD("<glow>ARROW", "&a+ 1 Player", "Editor:minplayerup", 0, (board -> {
        if (board.getMinPlayers() < board.getMaxPlayers()) {
            board.setMinPlayers(board.getMinPlayers() + 1);
            return true;
        }
        return false;
    })),
    MIN_PLAYERS_REMOVE("ARROW", "&c- 1 Player", "Editor:minplayerdown", 2, (board -> {
        if (board.getMinPlayers() > 2) {
            board.setMinPlayers(board.getMinPlayers() - 1);
            return true;
        }
        return false;
    })),

    MAX_PLAYERS("PLAYER_HEAD_ITEM", "&fMax players: &a{maxPlayers}", "Editor", 1, null),
    MAX_PLAYERS_ADD("<glow>ARROW", "&a+ 1 Player", "Editor:maxplayerup", 0, (board -> {
        if (board.getMaxPlayers() < 8) {
            board.setMaxPlayers(board.getMaxPlayers() + 1);
            return true;
        }
        return false;
    })),
    MAX_PLAYERS_REMOVE("ARROW", "&c- 1 Player", "Editor:maxplayerdown", 2, (board -> {
        if (board.getMaxPlayers() > board.getMinPlayers()) {
            board.setMaxPlayers(board.getMaxPlayers() - 1);
            return true;
        }
        return false;
    })),

    TIME_PER_ROUND("CLOCK", "&fTime per Player Round: &a{roundTime}", "Editor", 1, null),
    TIME_PER_ROUND_ADD("<glow>ARROW", "&a+ 5 Seconds", "Editor:playerroundtimeup", 0, (board -> {
        if (board.getPlayerRoundTime() < 120) {
            board.setPlayerRoundTime(board.getPlayerRoundTime() + 5);
            return true;
        }
        return false;
    })),
    TIME_PER_ROUND_REMOVE("ARROW", "&c- 5 Seconds", "Editor:playerroundtimedown", 2, (board -> {
        if (board.getPlayerRoundTime() > 30) {
            board.setPlayerRoundTime(board.getPlayerRoundTime() - 5);
            return true;
        }
        return false;
    })),

    LOBBY_TIME("CLOCK", "&fLobby waiting timer: &a{lobbyTime}", "Editor", 1, null),
    LOBBY_TIME_ADD("<glow>ARROW", "&a+ 5 Seconds", "Editor:lobbywaittimerup", 0, (board -> {
        if (board.getLobbyWaitTimer() < 120) {
            board.setLobbyWaitTimer(board.getLobbyWaitTimer() + 5);
            return true;
        }
        return false;
    })),
    LOBBY_TIME_REMOVE("ARROW", "&c- 5 Seconds", "Editor:lobbywaittimerdown", 2, (board -> {
        if (board.getLobbyWaitTimer() > 10) {
            board.setLobbyWaitTimer(board.getLobbyWaitTimer() - 5);
            return true;
        }
        return false;
    })),

    //todo is it okay to be +-5 rounds?
    MAX_ROUNDS("COMPASS", "&fMax rounds: &a{maxRounds}", "Editor", 1, null),
    MAX_ROUNDS_ADD("<glow>ARROW", "&a+ 5 Rounds", "Editor:maxroundsup", 0, (board -> {
        if (board.getMaxRounds() < 120) {
            board.setMaxRounds(board.getMaxRounds() + 5);
            return true;
        }
        return false;
    })),
    MAX_ROUNDS_REMOVE("ARROW", "&c- 5 Rounds", "Editor:maxroundsdown", 2, (board -> {
        if (board.getMaxRounds() > 5) {
            board.setMaxRounds(board.getMaxRounds() - 5);
            return true;
        }
        return false;
    })),
    ;

    private final String item;
    private final String name;
    private final String tag;
    private final int row;
    private final Predicate<Board> func;

    BoardGui(String item, String name, String tag, int row, Predicate<Board> func) {
        this.item = item;
        this.name = name;
        this.tag = tag;
        this.row = row * 9;
        this.func = func;
    }

    public String getItem() {
        return item;
    }

    public String getName() {
        return name;
    }

    public String getTag() {
        return tag;
    }

    public int getRow() {
        return row;
    }

    public Predicate<Board> getFunc() {
        return func;
    }
}
