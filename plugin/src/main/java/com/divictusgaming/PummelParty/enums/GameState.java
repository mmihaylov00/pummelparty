package com.divictusgaming.PummelParty.enums;

public enum GameState {
    DISABLED("Disabled"), // game not ready/disabled
    STARTING("Starting"), // the game has more >= the min players and it's starting soon
    AVAILABLE("Available"), // the game has < than the min players
    IN_GAME("In Game"), // the game has started
    ;
    private final String text;

    GameState(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
