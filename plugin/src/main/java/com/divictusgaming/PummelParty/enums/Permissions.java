package com.divictusgaming.PummelParty.enums;


public enum Permissions {
    FULL("pummelparty.full"),
    SETUP_BOARD_WIZARD("pummelparty.setup.board");
    private final String permission;

    Permissions(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
