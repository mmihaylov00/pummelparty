package com.divictusgaming.PummelParty.gadgets;

public enum GadgetsEnum {
    ANVIL(new AnvilGadget()),
    //todo add all gadgets
    ;

    private final Gadget gadget;

    GadgetsEnum(Gadget gadget) {
        this.gadget = gadget;
    }

    public Gadget getGadget() {
        return gadget;
    }
}
