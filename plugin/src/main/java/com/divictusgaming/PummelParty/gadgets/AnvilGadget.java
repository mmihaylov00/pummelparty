package com.divictusgaming.PummelParty.gadgets;

import com.divictusgaming.PummelParty.PummelPartyMain;
import com.divictusgaming.PummelParty.util.CommonUtils;
import com.divictusgaming.PummelParty.util.UMaterials;
import com.divictusgaming.PummelParty.util.particles.FastParticle;
import com.divictusgaming.PummelParty.util.particles.ParticleType;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.BlockIterator;

import java.util.ArrayList;
import java.util.List;

public class AnvilGadget implements Gadget {
    @SuppressWarnings("deprecation")
    @Override
    public void execute(PlayerInteractEvent event) {
        if (!event.getAction().equals(Action.LEFT_CLICK_AIR) && !event.getAction().equals(Action.LEFT_CLICK_BLOCK)) //test correct action
            return;
        if (!isGadget(event.getPlayer().getItemInHand())) //test if gadget used is correct
            return;
        Player executor = event.getPlayer();
        if (executor.hasMetadata("PummelParty.GadgetUsing"))    //test if player is using any gadget
            return;
        if (getUses(event.getPlayer().getItemInHand()) <= 0) //if player is out of uses
            return;
        event.getPlayer().setItemInHand(getItem(getUses(event.getPlayer().getItemInHand()) - 1));    //update the gadget item
        event.setCancelled(true);
        //add gadget metadata
        executor.setMetadata("PummelParty.GadgetUsing", new FixedMetadataValue(PummelPartyMain.getPlugin(), "anvil"));
        //start timer
        new BukkitRunnable() {
            int totalTime = 20 * 2;
            int currentTime = 0;
            Block lookingAt;

            @Override
            public void run() {
                //check if time isnt up
                if (currentTime < totalTime) {
                    //get block he is looking at(using block iterator)
                    BlockIterator bi = new BlockIterator(executor, 50); //50 is the radius limit (to prevent looking at sky)
                    Block lastBlock = bi.next();
                    while (bi.hasNext()) {
                        lastBlock = bi.next();
                        if (lastBlock.getType() == Material.AIR) //if looking at air
                        {
                            continue;
                        }
                        break;
                    }
                    lookingAt = lastBlock;
                    //spawn crosshair on the location
                    crosshair(executor, lookingAt, currentTime);
                    currentTime++;
                } else {
                    //spawn anvil at this location
                    dropAnvil(lookingAt);
                    //remove metadata
                    executor.removeMetadata("PummelParty.GadgetUsing", PummelPartyMain.getPlugin());
                    this.cancel();
                }
            }
        }.runTaskTimer(PummelPartyMain.getPlugin(), 0, 1);
    }

    @Override
    public ItemStack getItem(int useAmount) {
        //create a lore for item
        List<String> lore = new ArrayList<String>();
        lore.add("&7Drop an anvil to where");
        lore.add("&7you're looking.");
        lore.add("&7Deals &c9-11 &7damage.");
        //create the itemstack
        ItemStack item = CommonUtils.getCustomItem("<glow>ANVIL", CommonUtils.chat("&c&lAnvil"), lore);
        //add nbt tag to it
        item = CommonUtils.setStringTag(item, "gadget:anvil:" + useAmount);
        item.setAmount(useAmount);
        return item;
    }

    @Override
    public boolean isGadget(ItemStack item) {
        //check if item isnt null
        if (item != null && !item.getType().equals(UMaterials.AIR.getMaterial())) {
            String tag = CommonUtils.getStringTag(item);    //get tag
            if (tag != null && tag.startsWith("gadget:anvil"))    //check tag
                return true;
        }
        return false;
    }

    private int getUses(ItemStack item) {
        if (isGadget(item)) {
            String tag = CommonUtils.getStringTag(item);
            return Integer.valueOf(tag.split(":")[2]);
        }
        return 0;
    }

    @SuppressWarnings("deprecation")
    private void dropAnvil(Block block) {
        FallingBlock anvil = block.getWorld().spawnFallingBlock(block.getLocation().clone().add(0, 20, 0), UMaterials.ANVIL.getMaterial(), (byte) 0);
        anvil.setDropItem(false);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (anvil.isOnGround()) {
                    if (anvil.getLocation().getBlock().getType().equals(UMaterials.ANVIL.getMaterial()))
                        anvil.getLocation().getBlock().setType(UMaterials.AIR.getMaterial());
                    //TODO get any npc at this location & damage it accordingly
                    this.cancel();
                }
            }
        }.runTaskTimer(PummelPartyMain.getPlugin(), 0, 1);
    }

    private void crosshair(Player player, Block block, int time) {
        //get color for particle
        Color color;
        if (time >= 35)
            color = CommonUtils.getColorFromName("red");
        else if (time < 35 && time >= 15) //3 sec remain
            color = CommonUtils.getColorFromName("orange");
        else    //more than 3 remain
            color = CommonUtils.getColorFromName("lime");
        //get location to spawn crosshair
        Location loc = block.getLocation().clone().add(0.5, 1, 0.5);
        //spawn croshair (move +-1 in both x & z axis)
        for (double i = -1; i <= 1; i += 0.2) {
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, loc.clone().add(i, 0, 0), 1, color);
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, loc.clone().add(0, 0, i), 1, color);
        }
    }
}
