package com.divictusgaming.PummelParty.gadgets;

import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public interface Gadget {
    void execute(PlayerInteractEvent event);

    ItemStack getItem(int useAmount);

    boolean isGadget(ItemStack item);
}
