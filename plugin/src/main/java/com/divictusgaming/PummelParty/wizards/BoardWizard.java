package com.divictusgaming.PummelParty.wizards;


import com.divictusgaming.PummelParty.cuboid.Cuboid;
import com.divictusgaming.PummelParty.PummelPartyMain;
import com.divictusgaming.PummelParty.util.CommonUtils;
import com.divictusgaming.PummelParty.util.JSONMessage;
import com.divictusgaming.PummelParty.util.particles.BorderParticles;
import com.divictusgaming.PummelParty.util.particles.FastParticle;
import com.divictusgaming.PummelParty.util.particles.ParticleType;
import com.divictusgaming.PummelParty.util.UMaterials;
import com.divictusgaming.PummelParty.board.Board;
import com.divictusgaming.PummelParty.board.Pad;
import com.divictusgaming.PummelParty.board.Padtype;
import com.divictusgaming.PummelParty.enums.BoardGui;
import com.divictusgaming.PummelParty.data.yaml.YAMLWorker;
import com.divictusgaming.PummelParty.data.yaml.dto.BoardYAMLConfiguration;
import com.divictusgaming.PummelParty.data.yaml.dto.MessagesYAMLConfiguration;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BoardWizard implements Listener {
    private final Map<UUID, String> setupPlayers;
    private final Map<UUID, Location> alphaLocations;
    private final Map<UUID, Location> bravoLocations;
    private final Map<UUID, Location> lobbySpawnLocations;
    private final Map<UUID, Board> setupBoards;
    private final Map<UUID, Board> editingBoards;

    public BoardWizard() {
        setupPlayers = new HashMap<>();
        alphaLocations = new HashMap<>();
        bravoLocations = new HashMap<>();
        setupBoards = new HashMap<>();
        editingBoards = new HashMap<>();
        lobbySpawnLocations = new HashMap<>();
        //this is only for displaying particles
        MessagesYAMLConfiguration messages = PummelPartyMain.getMessages();
        new BukkitRunnable() {
            @Override
            public void run() {
                for (UUID uuid : alphaLocations.keySet()) {
                    Player player = Bukkit.getPlayer(uuid);
                    if (player != null) {
                        if (setupPlayers.containsKey(player.getUniqueId()) && alphaLocations.containsKey(player.getUniqueId())) {
                            Location newCorner;
                            if (bravoLocations.containsKey(player.getUniqueId()))
                                newCorner = bravoLocations.get(player.getUniqueId());
                            else
                                newCorner = player.getLocation();
                            if (!newCorner.getWorld().equals(alphaLocations.get(player.getUniqueId()).getWorld())) {
                                player.sendMessage(messages.getBoardPrefix() + messages.getBoardWizardCanceled());
                                cancelCreation(player);
                                return;
                            }
                            //try & make a boundary of cuboid using particles
                            BorderParticles.sendBoundary(player, alphaLocations.get(player.getUniqueId()), newCorner);
                        }
                        if (setupBoards.containsKey(player.getUniqueId()) && setupBoards.get(player.getUniqueId()) != null) {
                            //try & send particles at different pads
                            Board board = setupBoards.get(player.getUniqueId());
                            for (Pad pad : board.getPads())
                                FastParticle.spawnParticle(player, ParticleType.REDSTONE, pad.getLocation().clone().add(0.5, 1, 0.5), 1,
                                        pad.getPadtype().getColor());
                            for (Pad pad : board.getGraveyards())
                                FastParticle.spawnParticle(player, ParticleType.REDSTONE, pad.getLocation().clone().add(0.5, 1, 0.5), 1,
                                        pad.getPadtype().getColor());
                        }
                    }
                }
            }
        }.runTaskTimer(PummelPartyMain.getPlugin(), 20, 5);
    }

    public void openEditor(Player player, Board board)    //open board editor gui
    {
        editingBoards.put(player.getUniqueId(), board);
        player.openInventory(getBoardGui(board));
    }

    public boolean startSetup(Player player, String boardName) {
        //check if this player is already creating a board
        if (setupPlayers.containsKey(player.getUniqueId()))
            return false;
        setupPlayers.put(player.getUniqueId(), boardName);
        return true;
        //add this player to setup players
    }

    public boolean cancelCreation(Player player) //function to abruptly cancel creation of board
    {
        if (setupPlayers.containsKey(player.getUniqueId())) {
            setupPlayers.remove(player.getUniqueId());
            alphaLocations.remove(player.getUniqueId());
            setupBoards.remove(player.getUniqueId());
            bravoLocations.remove(player.getUniqueId());
            lobbySpawnLocations.remove(player.getUniqueId());
            return true;
        } else {
            alphaLocations.remove(player.getUniqueId());
            setupBoards.remove(player.getUniqueId());
            bravoLocations.remove(player.getUniqueId());
            lobbySpawnLocations.remove(player.getUniqueId());
            return false;
        }
    }

    public void setWaitingLobby(Player player) {
        MessagesYAMLConfiguration messages = PummelPartyMain.getMessages();
        lobbySpawnLocations.put(player.getUniqueId(), player.getLocation());
        //send message for selecting other corner point
        JSONMessage.create(messages.getBoardPrefix() + messages.getBoardWizardFirstCorner())
                .tooltip(messages.getBoardWizardSetCornerTooltip()).runCommand("/pp board start-corner").send(player);
    }

    //event is being provided by EventManager class ( no need for @EventHandler annotation)
    public void blockSelectHandle(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (!setupPlayers.containsKey(player.getUniqueId()))
            return;
        if (!event.getAction().equals(Action.LEFT_CLICK_BLOCK))
            return;
        if (event.getClickedBlock() == null)
            return;
        if (lobbySpawnLocations.get(player.getUniqueId()) == null) {
            return;
        }
        MessagesYAMLConfiguration messages = PummelPartyMain.getMessages();
        //check if player is to select first location
        if (!alphaLocations.containsKey(player.getUniqueId()) && !setupBoards.containsKey(player.getUniqueId())) {
            event.setCancelled(true);
            alphaLocations.put(player.getUniqueId(), event.getClickedBlock().getLocation());
            //send message for selecting other corner point
            player.sendMessage(messages.getBoardPrefix() + messages.getBoardWizardFirstCornerSet());
            JSONMessage.create(messages.getBoardPrefix() + messages.getBoardWizardSecondCorner())
                    .tooltip(messages.getBoardWizardSetCornerTooltip()).runCommand("/pp board select-corner").send(player);
            return;
        }
        //check if player is to select second location
        if (!bravoLocations.containsKey(player.getUniqueId()) && !setupBoards.containsKey(player.getUniqueId())) {
            event.setCancelled(true);
            bravoLocations.put(player.getUniqueId(), event.getClickedBlock().getLocation());
            createBoard(player, messages);

            JSONMessage.create(messages.getBoardPrefix() + messages.getBoardWizardFinish())
                    .tooltip(messages.getBoardWizardFinishTooltip()).runCommand("/pp board finish-selection").send(player);
            return;
        }
        //check if player is setting spawnpoints & graveyards
        if (alphaLocations.containsKey(player.getUniqueId()) && bravoLocations.containsKey(player.getUniqueId()) &&
                setupBoards.containsKey(player.getUniqueId()) && event.getClickedBlock() != null) {
            //get clicked block
            Block block = event.getClickedBlock();
            //get board
            Board board = setupBoards.get(player.getUniqueId());
            if (board != null) {
                //check if clicked correct block
                if (Padtype.SPAWN_FIELD.getUmaterial().equals(UMaterials.getItem(block)) || Padtype.GRAVEYARD.getUmaterial().equals(UMaterials.getItem(block))) {
                    event.setCancelled(true);
                    //check if clicked block is in cuboid
                    if (!board.getCuboid().isInCuboid(block.getLocation())) {
                        player.sendMessage(messages.getBoardPrefix() + messages.getBoardWizardOutsideBorder());
                        return;
                    }
                    //test if spawn is selected
                    if (UMaterials.getItem(block).equals(Padtype.SPAWN_FIELD.getUmaterial())) {
                        //check if spawn was previously set
                        if (board.getSpawn() != null) {
                            Location spawn = board.getSpawn().getLocation();
                            player.sendMessage(messages.getBoardPrefix() +
                                    messages.getBoardWizardSpawnPrevious(spawn.getX() + "", spawn.getY() + "", spawn.getZ() + ""));
                            player.sendMessage(messages.getBoardPrefix() + messages.getBoardWizardSpawnMoved());
                            board.removeSpawn();
                        }
                        board.setSpawn(block);
                        Location spawn = board.getSpawn().getLocation();
                        player.sendMessage(messages.getBoardPrefix() + messages.getBoardWizardSpawnSet(spawn.getX() + "", +spawn.getY() + "", spawn.getZ() + ""));
                        JSONMessage.create(messages.getBoardPrefix() + messages.getBoardWizardFinish())
                                .tooltip(messages.getBoardWizardFinishTooltip()).runCommand("/pp board finish").send(player);
                        return;
                    }
                    //test if grave is selected
                    if (UMaterials.getItem(block).equals(Padtype.GRAVEYARD.getUmaterial())) {
                        //check if grave was previously set
                        for (Pad grave : board.getGraveyards())
                            if (grave.getLocation().equals(block.getLocation())) {
                                player.sendMessage(messages.getBoardPrefix() + messages.getBoardWizardGraveAlreadySet());
                                return;
                            }
                        board.addGraveyard(block);
                        Location grave = block.getLocation();
                        player.sendMessage(messages.getBoardPrefix() +
                                messages.getBoardWizardGraveyardAdded(grave.getX() + "", grave.getY() + "", grave.getZ() + ""));
                        player.sendMessage(messages.getBoardPrefix() + messages.getBoardWizardGraveyardsSize(board.getGraveyards().size() + ""));
                        JSONMessage.create(messages.getBoardPrefix() + messages.getBoardWizardFinish())
                                .tooltip(messages.getBoardWizardFinishTooltip()).runCommand("/pp board finish").send(player);
                    }
                }
            }
        }
    }

    private void createBoard(Player player, MessagesYAMLConfiguration messages) {
        Board board = new Board(setupPlayers.get(player.getUniqueId()), Cuboid.create(alphaLocations.get(player.getUniqueId()),
                bravoLocations.get(player.getUniqueId())), lobbySpawnLocations.get(player.getUniqueId()));
        setupBoards.put(player.getUniqueId(), board);
        player.sendMessage(messages.getBoardPrefix() + messages.getBoardWizardSecondCornerSet());
        player.sendMessage(messages.getBoardPrefix() + messages.getBoardWizardSetSpawn(Padtype.SPAWN_FIELD.getUmaterial().getVersionColoredName()));
        player.sendMessage(messages.getBoardPrefix() + messages.getBoardWizardSetGraveyard(Padtype.GRAVEYARD.getUmaterial().getVersionColoredName()));
    }

    public void setBravoLocation(Player player)    //sets second location if it isnt set
    {
        if (!setupPlayers.containsKey(player.getUniqueId()))
            return;
        if (alphaLocations.containsKey(player.getUniqueId()) && !bravoLocations.containsKey(player.getUniqueId()) && !setupBoards.containsKey(player.getUniqueId())) {
            MessagesYAMLConfiguration messages = PummelPartyMain.getMessages();
            bravoLocations.put(player.getUniqueId(), player.getLocation());
            createBoard(player, messages);
            JSONMessage.create(messages.getBoardPrefix() + messages.getBoardWizardFinish())
                    .tooltip(messages.getBoardWizardFinishTooltip()).runCommand("/pp board finish").send(player);
        }
    }

    public void setAlphaLocations(Player player)    //sets first location if it isnt set
    {
        if (!setupPlayers.containsKey(player.getUniqueId()))
            return;
        if (!alphaLocations.containsKey(player.getUniqueId()) && !setupBoards.containsKey(player.getUniqueId())) {
            MessagesYAMLConfiguration messages = PummelPartyMain.getMessages();
            alphaLocations.put(player.getUniqueId(), player.getLocation());
            player.sendMessage(messages.getBoardPrefix() + messages.getBoardWizardFirstCornerSet());
            JSONMessage.create(messages.getBoardPrefix() + messages.getBoardWizardSecondCorner())
                    .tooltip(messages.getBoardWizardSetCornerTooltip()).runCommand("/pp board select-corner").send(player);
        }
    }

    public String getBoardName(Player player) {
        if (setupBoards.containsKey(player.getUniqueId()) && setupBoards.get(player.getUniqueId()) != null)
            return setupBoards.get(player.getUniqueId()).getMapName();
        return "";
    }

    public boolean checkFinished(Player player)//check if board was properly finished creation
    {
        if (!setupBoards.containsKey(player.getUniqueId()))
            return false;
        if (setupBoards.get(player.getUniqueId()) == null)
            return false;
        if (setupBoards.get(player.getUniqueId()).getSpawn() == null)
            return false;
        if (setupBoards.get(player.getUniqueId()).getGraveyards().size() <= 0)
            return false;
        //get board & save it with the rest
        Board board = setupBoards.get(player.getUniqueId());
        HashMap<String, String> placeholders = new HashMap<>();
        placeholders.put("name", board.getMapName());
        YAMLWorker.saveFile(new BoardYAMLConfiguration(board), placeholders);
        board.reset();
        PummelPartyMain.getBoards().put(board.getMapName(), board);
        return true;
    }

    private final BoardGui[] guiValues = BoardGui.values();

    private Inventory getBoardGui(Board board) {
        Inventory inv = Bukkit.createInventory(null, 9 * 3, CommonUtils.chat("Board Editor"));
        //set background
        ItemStack background = CommonUtils.getCustomItem("BLACK_STAINED_GLASS_PANE", "&fEditing &a" + board.getMapName(), new ArrayList<String>());
        background = CommonUtils.setStringTag(background, "Editor");    //add tags so we know this item is part of gui
        for (int i = 0; i < 9 * 3; i++)
            inv.setItem(i, background);
        //set time part
        int currentColumn = 0;
        //looping BoardGui enums for less code repetition
        for (int i = 0; i < guiValues.length; i++) {
            BoardGui guiItem = guiValues[i];
            String lore = guiItem.getName();
            lore = lore.replace("\\{time}", CommonUtils.getTimeFormat(board.getPlayTime()));
            lore = lore.replace("\\{minPlayers}", "" + board.getMinPlayers());
            lore = lore.replace("\\{maxPlayers}", "" + board.getMaxPlayers());
            lore = lore.replace("\\{roundTime}", CommonUtils.getTimeFormat(board.getPlayerRoundTime()));
            lore = lore.replace("\\{lobbyTime}", CommonUtils.getTimeFormat(board.getLobbyWaitTimer()));
            lore = lore.replace("\\{maxRounds}", "" + board.getMaxRounds());
            ItemStack item = CommonUtils.getCustomItem(guiItem.getItem(), CommonUtils.chat(lore), new ArrayList<>());
            item = CommonUtils.setStringTag(item, guiItem.getTag());    //add tags so we know this item is part of gui
            inv.setItem(guiItem.getRow() + currentColumn, item);
            if ((i + 1) % 3 == 0) currentColumn++;
        }

        //set delete part
        ItemStack delete = CommonUtils.getCustomItem("TNT", CommonUtils.chat("&cDelete Board"), new ArrayList<>());
        delete = CommonUtils.setStringTag(delete, "Editor:delete");
        inv.setItem(16, delete);
        return inv;
    }


    //event is being provided by EventManager class ( no need for @EventHandler annotation)
    public void guiActionHandle(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (event.getRawSlot() > -1) //test if clicked inside the inventory
        {
            ItemStack item = event.getCurrentItem();
            if (item == null || item.getType().equals(UMaterials.AIR.getMaterial())) //test if clicked an item
                return;
            String tag = CommonUtils.getStringTag(item);
            if (tag != null && !tag.equals("") && tag.startsWith("Editor")) //if item was tagged with editor (i.e gui item)
            {
                event.setCancelled(true);
                if (!editingBoards.containsKey(player.getUniqueId())) //if no board is saved against player
                    return;
                Board board = editingBoards.get(player.getUniqueId());

                boolean saveFile = false;
                if (!tag.equals("Editor")) {
                    for (BoardGui guiValue : guiValues) {
                        if (guiValue.getFunc() != null && guiValue.getTag().equals(tag)) {
                            saveFile = guiValue.getFunc().test(board);
                        }
                    }
                    if (tag.equals("Editor:delete")) {
                        //remove this from all saved boards
                        PummelPartyMain.getBoards().remove(board.getMapName());
                        //delete the file as well
                        HashMap<String, String> placeholders = new HashMap<>();
                        placeholders.put("name", board.getMapName());
                        YAMLWorker.loadFile(new BoardYAMLConfiguration(board), placeholders).delete();
                        //remove from editing
                        editingBoards.remove(player.getUniqueId());
                        //close the inv
                        player.closeInventory();
                        return;
                    }
                }

                if (saveFile) {
                    HashMap<String, String> placeholders = new HashMap<>();
                    placeholders.put("name", board.getMapName());
                    YAMLWorker.saveFile(new BoardYAMLConfiguration(board), placeholders);
                    player.openInventory(getBoardGui(board));
                }
            }
        }
    }

}
