package com.divictusgaming.PummelParty.characters;

import com.divictusgaming.PummelParty.PummelPartyMain;
import com.divictusgaming.PummelParty.board.Board;
import com.divictusgaming.PummelParty.data.PlayerConfiguration;
import com.divictusgaming.PummelParty.gadgets.GadgetsEnum;
import com.divictusgaming.PummelParty.util.Dice;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerCharacter {
    //TODO add a playerEntity which will act as player's character
    //TODO add functions to move that entity to locations
    //TODO add functions to set skins of that entity
    //TODO add function to make entity jump (for when rolling dice)
    private String player;    //save
    private final UUID uuid;    //save

    private Location location;    //change per game
    private int gameKeys;    //change per game
    private int health;        //change per game
    private int gameTrophies;    //change per game

    private int totalKeys = 0;    //save; total amount of keys collected
    private int totalTrophies = 0;    //save; total amount of trophies collected
    private int wins = 0;    //save
    private int totalGames = 0;    //save;

    private String board;
    private int diceThrow;
    private ChatColor gameColor;
    private final Map<GadgetsEnum, Integer> gadgets;
    private int minigamePoints = 0;
    private boolean isUpdated = false;

    public void joinBoard(Board board) {
        this.board = board.getMapName();
    }

    public void leaveBoard() {
        this.board = null;
    }

    public Board getBoard() {
        return PummelPartyMain.getBoards().get(board);
    }

    public PlayerCharacter(Player player) {
        this.gameColor = null;
        this.player = player.getName();
        this.uuid = player.getUniqueId();
        this.diceThrow = 0;
        this.gadgets = new HashMap<>();

        new BukkitRunnable() {
            @Override
            public void run() {
                PlayerConfiguration playerConfiguration = PummelPartyMain.getDatabase().loadData(player.getUniqueId());
                totalKeys = playerConfiguration.getKeys();
                totalTrophies = playerConfiguration.getTrophies();
                wins = playerConfiguration.getWins();
                totalGames = playerConfiguration.getTotalGames();
            }
        }.runTask(PummelPartyMain.getPlugin());
    }

    //use this whenever player playes a new game
    public void start(Location location) {
        this.location = location;
        this.health = 20;
    }

    //saves current stats
    public void save() {
        PlayerCharacter p = this;
        new BukkitRunnable() {
            @Override
            public void run() {
                PummelPartyMain.getDatabase().saveData(p);
            }
        }.runTask(PummelPartyMain.getPlugin());
    }

    public Location getLocation() {
        return location;
    }

    public int getWins() {
        return this.wins;
    }

    public void setWins(int wins) {
        isUpdated = true;
        this.wins = wins;
    }

    public int getTotalGames() {
        return totalGames;
    }

    public void setLostGames(int lost) {
        this.totalGames = lost;
    }

    public void setLocation(Location location) //TODO change this to move npc instead
    {
        this.location = location;
    }

    public UUID getPlayerUUID() {
        return uuid;
    }

    public int getTotalKeys() {
        return totalKeys;
    }

    public void setTotalKeys(int totalKeys) {
        isUpdated = true;
        this.totalKeys = totalKeys;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getTotalTrophies() {
        return totalTrophies;
    }

    public void setTotalTrophies(int totalTrophies) {
        isUpdated = true;
        this.totalTrophies = totalTrophies;
    }

    public String getPlayer() {
        return this.player;
    }

    public int getGameKeys() {
        return gameKeys;
    }

    public int getGameTrophies() {
        return gameTrophies;
    }

    public int getDiceThrow() {
        return diceThrow;
    }

    public void throwDice() {
        Player player = Bukkit.getPlayer(uuid);
        Dice.animateThrowDice(player);
        new BukkitRunnable() {
            @Override
            public void run() {
                diceThrow = Dice.getLastRoll(player);
            }
        }.runTaskLater(PummelPartyMain.getPlugin(), Dice.getAnimationTicks());
    }

    public ChatColor getGameColor() {
        return gameColor;
    }

    public void setGameColor(ChatColor gameColor) {
        this.gameColor = gameColor;
    }

    public int getMinigamePoints() {
        return minigamePoints;
    }

    public void setMinigamePoints(int minigamePoints) {
        this.minigamePoints = minigamePoints;
    }

    public boolean isUpdated() {
        return isUpdated;
    }
}
