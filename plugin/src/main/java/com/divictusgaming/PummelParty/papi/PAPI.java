package com.divictusgaming.PummelParty.papi;

import com.divictusgaming.PummelParty.PummelPartyMain;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class PAPI extends me.clip.placeholderapi.expansion.PlaceholderExpansion {
    @Override
    public String getIdentifier() {
        return "pummelparty";
    }

    @Override
    public String getAuthor() {
        return "DivictusGaming";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String onPlaceholderRequest(Player player, String identifier) {
        return getValue(player, identifier);
    }

    //PlaceholderAPI placeholders:
    //%pummelparty_keys% - player's total keys
    //%pummelparty_trophies% - player's trophies
    //%pummelparty_games% - player's games
    //%pummelparty_wins% - player's wins
    private String getValue(OfflinePlayer player, String identifier) {
        String[] pl = identifier.split("_");
        if (pl.length < 2) {
            return "";
        }
        switch (pl[1]) {
            case "keys":
                return PummelPartyMain.getCharacterStorage().getCharacter(player.getUniqueId()).getTotalKeys() + "";
            case "trophies":
                return PummelPartyMain.getCharacterStorage().getCharacter(player.getUniqueId()).getTotalTrophies() + "";
            case "games":
                return PummelPartyMain.getCharacterStorage().getCharacter(player.getUniqueId()).getTotalGames() + "";
            case "wins":
                return PummelPartyMain.getCharacterStorage().getCharacter(player.getUniqueId()).getWins() + "";
        }

        return "";
    }

    @Override
    public String onRequest(OfflinePlayer player, String identifier) {
        return getValue(player, identifier);
    }
}
