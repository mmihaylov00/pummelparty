package com.divictusgaming.PummelParty.storage;

import com.divictusgaming.PummelParty.characters.PlayerCharacter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class CharacterStorage {
    private final HashMap<UUID, PlayerCharacter> playerCharacters;

    public CharacterStorage() {
        this.playerCharacters = new HashMap<>();
        loadOnlinePlayers();
    }

    public void loadOnlinePlayers() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            playerCharacters.put(player.getUniqueId(), new PlayerCharacter(player));
            playerCharacters.get(player.getUniqueId()).save();
        }
    }

    public void saveAll() {
        for (PlayerCharacter character : playerCharacters.values())
            character.save();
    }

    public PlayerCharacter getCharacter(UUID p) {
        PlayerCharacter character = playerCharacters.get(p);

        if (character == null) {
            Player player = Bukkit.getPlayer(p);
            if (player == null) return null;

            character = new PlayerCharacter(player);
            character.save();
        }

        return character;
    }

    public PlayerCharacter getCharacter(Player p) {
        PlayerCharacter character = playerCharacters.get(p.getUniqueId());
        if (character == null) {
            character = new PlayerCharacter(p);
            character.save();
        }
        return character;
    }

    public void unloadCharacter(Player player) {
        PlayerCharacter p = playerCharacters.remove(player.getUniqueId());
        if (p != null) p.save();
    }

    public void unloadCharacter(UUID player) {
        PlayerCharacter p = playerCharacters.remove(player);
        if (p != null) p.save();
    }

    public void saveCharacter(Player player) {
        PlayerCharacter p = playerCharacters.get(player.getUniqueId());
        if (p != null) p.save();
    }

    public void saveCharacter(UUID player) {
        PlayerCharacter p = playerCharacters.get(player);
        if (p != null) p.save();
    }
}
