package com.divictusgaming.PummelParty.storage;

import com.divictusgaming.PummelParty.PummelPartyMain;
import com.divictusgaming.PummelParty.board.Board;
import com.divictusgaming.PummelParty.data.yaml.YAMLWorker;
import com.divictusgaming.PummelParty.data.yaml.dto.BoardYAMLConfiguration;
import org.bukkit.Bukkit;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class BoardStorage {
    private HashMap<String, Board> boards;

    public BoardStorage() {
        this.boards = new HashMap<>();
        loadAllBoards();
    }

    public HashMap<String, Board> getBoards() {
        return boards;
    }

    private void loadAllBoards() {
        Bukkit.getLogger().info("Loading Pre-Saved Boards.");
        //first we need the folder location (if they are in separate folder)
        String fileLocation = YAMLWorker.getFileLocation(new BoardYAMLConfiguration());
        //then we get all the filenames stored in that location
        File folder = new File(PummelPartyMain.getPluginsFolder() + fileLocation + "/");
        String[] fileNames = folder.list();
        ArrayList<String> names = new ArrayList<>();

        if (fileNames != null) {
            for (String s : fileNames) {
                if (s.endsWith(".yml"))
                    names.add(s.replace(".yml", ""));
                else if (s.endsWith("."))
                    names.add(s.replace(".", ""));
                else
                    names.add(s);
            }
        }

        //now we try & get board class from file
        for (String name : names) {
            HashMap<String, String> placeholder = new HashMap<>();
            placeholder.put("name", name);
            BoardYAMLConfiguration boardConfig;
            try {
                boardConfig = BoardYAMLConfiguration.class.newInstance();
                YAMLWorker.readFile(boardConfig, placeholder);
                Board board = boardConfig.getBoard();
                board.reset();
                boards.put(name, board);
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        Bukkit.getLogger().info("Loaded " + boards.size() + " boards.");
    }
}
