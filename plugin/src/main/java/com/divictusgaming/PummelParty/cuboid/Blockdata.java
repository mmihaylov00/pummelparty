package com.divictusgaming.PummelParty.cuboid;


import com.divictusgaming.PummelParty.util.CommonUtils;
import com.divictusgaming.PummelParty.util.StringFormater;
import com.divictusgaming.PummelParty.util.UMaterials;
import com.divictusgaming.PummelParty.exceptions.InvalidCuboidException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Skull;
import org.bukkit.block.data.BlockData;

import static com.divictusgaming.PummelParty.util.ReflectionUtils.*;

public class Blockdata {
    private Location location = null;
    private UMaterials type = null;
    private Object data = null;    //blockData in case of 1.13 & above & byte incase of below
    private Block block = null;
    private String skullOwner;

    public static Blockdata deserialize(String string) {
        JsonParser parser = new JsonParser();
        JsonObject object = (JsonObject) parser.parse(string);
        if (object.get("SavedVersion") == null || object.get("Location") == null)
            throw new InvalidCuboidException("Cant deserialize the string " + string);
        if (object.get("SavedVersion").getAsDouble() != CommonUtils.getServerVersion())
            throw new InvalidCuboidException("String made for different Version.");
        Blockdata bd = new Blockdata();

        //getlocation & block
        String locationString = object.get("Location").getAsString();
        String[] split = locationString.split(";");
        bd.location = new Location(Bukkit.getWorld(split[0]),
                Double.parseDouble(split[1]),
                Double.parseDouble(split[2]),
                Double.parseDouble(split[3]));
        bd.block = bd.location.getBlock();
        //get the type
        if (object.get("Type") != null)
            bd.type = UMaterials.valueOf(object.get("Type").getAsString());
        else
            bd.type = UMaterials.getItem(bd.block);
        if (CommonUtils.getServerVersion() >= 13) //1.13 or above
        {
            //get data
            if (object.get("Data") != null)
                bd.data = Bukkit.createBlockData(object.get("Data").getAsString());
            else
                bd.data = bd.block.getBlockData();
        } else {
            //get data
            if (object.get("Data") != null)
                bd.data = Byte.valueOf(object.get("Data").getAsString());
            else
                bd.data = bd.block.getData();
            //get skullOwner
            if (object.get("SkullOwner") != null)
                bd.skullOwner = object.get("SkullOwner").getAsString();
        }
        //update this block
        bd.update();
        return bd;
    }

    public static Blockdata adapt(Block block) {
        Blockdata bd = new Blockdata();
        if (CommonUtils.getServerVersion() >= 13) //1.13 or above
        {
            bd.location = block.getLocation();
            bd.block = block;
            bd.type = UMaterials.getItem(block);
        } else {
            bd.location = block.getLocation();
            bd.data = block.getData();
            bd.type = UMaterials.getItem(block);
            bd.block = block;
            if (block.getState() instanceof Skull)
                bd.skullOwner = ((Skull) block.getState()).getOwner();
        }
        return bd;
    }

    public boolean compareWith(Block block) {
        return Blockdata.adapt(block).equals(this);
    }

    public void update() {
        if (block == null || location == null)
            throw new InvalidCuboidException("BlockData not setup properly");
        if (!location.getBlock().equals(block))
            throw new InvalidCuboidException("Block & Location mismatched.");
        if (CommonUtils.getServerVersion() >= 13) //1.13 or above
        {
            //set type & data only
            block.setType(type.getMaterial());
            block.setBlockData((BlockData) data);
        } else {
            //set all available data
            block.setType(type.getMaterial());
            //set data using reflections (block.setData(byte data) was removed in 1.13)
            setdata((byte) data);
            block.getState().update();
            if (block.getState() instanceof Skull && skullOwner != null)    //set skullOwner
                ((Skull) block.getState()).setOwner(skullOwner);
        }
        //update block
        block.getState().update();
    }

    private void setdata(byte info) {
        RefClass Block = getRefClass("{cb}.block.CraftBlock");
        RefMethod setData = Block.getMethod("setData", byte.class);
        setData.of(block).call(data);
    }

    public String serializeToString() {
        if (block == null || location == null)
            throw new InvalidCuboidException("BlockData not setup properly");
        JsonObject object = new JsonObject();
        //set savedVersion
        object.addProperty("SavedVersion", CommonUtils.getServerVersion());
        //set block location to data
        object.addProperty("Location", StringFormater.locationToString(block.getLocation(), false));
        //set other data (based on server version)
        if (type != null)
            object.addProperty("Type", type.name());
        if (data != null)
            object.addProperty("Data", getDataObject());
        if (skullOwner != null)
            object.addProperty("SkullOwner", skullOwner);
        return object.toString();
    }

    private String getDataObject() {
        String data = "";
        if (CommonUtils.getServerVersion() >= 13) //1.13 or above
        {
            data = ((BlockData) this.data).getAsString();
        } else {
            data = String.valueOf((byte) this.data);
        }
        return data;
    }

    public Location getLocation() {
        return this.location;
    }

    public Block getBlock() {
        return this.block;
    }

    public UMaterials getType() {
        return this.type;
    }

    public Object getData() {
        return this.data;
    }
}
