package com.divictusgaming.PummelParty.board;

import com.divictusgaming.PummelParty.cuboid.Cuboid;
import com.divictusgaming.PummelParty.PummelPartyMain;
import com.divictusgaming.PummelParty.characters.PlayerCharacter;
import com.divictusgaming.PummelParty.enums.GameState;
import com.divictusgaming.PummelParty.enums.ScoreboardType;
import com.divictusgaming.PummelParty.exceptions.BlockNotPadException;
import com.divictusgaming.PummelParty.data.yaml.dto.ConfigYAMLConfiguration;
import com.divictusgaming.PummelParty.data.yaml.dto.MessagesYAMLConfiguration;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;
import java.util.function.Consumer;

public class Board {
    private final String mapName;
    private Pad spawn;
    private int playTime;
    private final List<Pad> graveyards;
    private List<Pad> pads;
    private final Map<UUID, PlayerCharacter> players;
    private final Cuboid cuboid;
    private int minPlayers;
    private int maxPlayers;
    private GameState state;
    private int gameTimer;
    private int playerRoundTime;
    private int lobbyWaitTimer;
    private int maxRounds;
    private Location lobbySpawn;
    private final Queue<PlayerCharacter> queue;

    public Board(String mapName, Cuboid cuboid, Location lobbySpawn) {
        this.mapName = mapName;
        this.pads = new ArrayList<>();
        this.players = new HashMap<>();
        this.graveyards = new ArrayList<>();
        this.cuboid = cuboid;
        playTime = 60 * 30;
        minPlayers = 2;
        maxPlayers = 8;
        state = GameState.DISABLED;
        playerRoundTime = 30;
        lobbyWaitTimer = 60;
        maxRounds = 15;
        this.lobbySpawn = lobbySpawn;
        queue = new ArrayDeque<>();
    }

    public void reset() {
        ConfigYAMLConfiguration config = PummelPartyMain.getConfiguration();
        for (UUID uuid : players.keySet()) {
            Player p = Bukkit.getPlayer(uuid);
            if (p != null) p.teleport(config.getSpawnLocation());
        }
        players.clear();
        resetLobby();
    }

    public void resetLobby() {
        state = GameState.AVAILABLE;
        gameTimer = lobbyWaitTimer;
        lobbyStarted = false;
        //we want to change back to the needed players scoreboard
        players.keySet().forEach(u -> {
            PummelPartyMain.getScoreboardManager()
                    .setScoreboard(u, ScoreboardType.WAITING_LOBBY);
        });
    }

    public void join(Player p) {
        MessagesYAMLConfiguration messages = PummelPartyMain.getMessages();
        //if the global spawn is not set, we don't want the players to be able to join
        if (PummelPartyMain.getConfiguration().getSpawnLocation() == null) {
            p.sendMessage(messages.getPrefix() + messages.getSpawnNotSet());
            return;
        }
        PlayerCharacter character = PummelPartyMain.getCharacterStorage().getCharacter(p);
        if (character.getBoard() != null) {
            p.sendMessage(messages.getPrefix() + messages.getBoardJoinAlreadyInBoard());
            return;
        }
        //if there is place and the game isn't running
        if (players.size() == maxPlayers) {
            p.sendMessage(messages.getPrefix() + messages.getBoardJoinIsFull());
            return;
        }
        if (state == GameState.IN_GAME || state == GameState.DISABLED) {
            p.sendMessage(messages.getPrefix() + messages.getBoardJoinAlreadyStarted());
            return;
        }
        //add the player to the game
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            PummelPartyMain.getINMSMain().hidePlayer(onlinePlayer, p, PummelPartyMain.getPlugin());
            PummelPartyMain.getINMSMain().hidePlayer(p, onlinePlayer, PummelPartyMain.getPlugin());
        }
        for (UUID uuid : players.keySet()) {
            Player gamePlayer = Bukkit.getPlayer(uuid);
            PummelPartyMain.getINMSMain().showPlayer(gamePlayer, p, PummelPartyMain.getPlugin());
            PummelPartyMain.getINMSMain().showPlayer(p, gamePlayer, PummelPartyMain.getPlugin());
        }
        players.put(p.getUniqueId(), character);
        character.joinBoard(this);
        sendBoardMessage(messages.getPrefix() + messages.getBoardPlayerJoined(p.getName(),
                players.size() + "", maxPlayers + ""));
        //change his scoreboard
        PummelPartyMain.getScoreboardManager().setScoreboard(p.getUniqueId(), ScoreboardType.WAITING_LOBBY);
        //tp him to lobby
        p.teleport(lobbySpawn);
        //start lobby counting
        if (players.size() == minPlayers && !lobbyStarted) {
            startLobbyTimer();
        }
    }

    public void leave(Player p) {
        //remove the player on leave
        players.remove(p.getUniqueId());
        PummelPartyMain.getCharacterStorage().getCharacter(p).leaveBoard();
        //we send a message to all players that he left
        MessagesYAMLConfiguration messages = PummelPartyMain.getMessages();
        ConfigYAMLConfiguration config = PummelPartyMain.getConfiguration();
        sendBoardMessage(messages.getPrefix() + messages.getBoardPlayerLeave(p.getName(),
                players.size() + "", maxPlayers + ""));
        if (p.isOnline()) {
            for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
                if (PummelPartyMain.getCharacterStorage().getCharacter(onlinePlayer).getBoard() == null) {
                    PummelPartyMain.getINMSMain().showPlayer(onlinePlayer, p, PummelPartyMain.getPlugin());
                    PummelPartyMain.getINMSMain().showPlayer(p, onlinePlayer, PummelPartyMain.getPlugin());
                }
            }
            for (UUID uuid : players.keySet()) {
                Player gamePlayer = Bukkit.getPlayer(uuid);
                PummelPartyMain.getINMSMain().hidePlayer(gamePlayer, p, PummelPartyMain.getPlugin());
                PummelPartyMain.getINMSMain().hidePlayer(p, gamePlayer, PummelPartyMain.getPlugin());
            }
        }
        //teleport to the lobby
        p.teleport(config.getSpawnLocation());
        //set his scoreboard to lobby
        PummelPartyMain.getScoreboardManager().setScoreboard(p.getUniqueId(), ScoreboardType.LOBBY);
    }

    private void startGame() {
        state = GameState.IN_GAME;

        //todo replace this message
        //todo change the teleport location when you figure it out
        setupPlayersColors();
        sendBoardMessage("GAME IS STARTING...",
                player -> PummelPartyMain.getScoreboardManager().setScoreboard(player.getUniqueId(), ScoreboardType.IN_GAME),
                player -> player.teleport(spawn.getLocation()));

        gameTimer = 15;
        new BukkitRunnable() {
            //update stuff every second
            @Override
            public void run() {
                //start the game if the timer is on 0
                if (gameTimer == 0) {
                    this.cancel();
                    forceThrowAll();
                } else if (checkIfAllThrown()) {
                    this.cancel();
                    startRound();
                }
                gameTimer--;
            }
        }.runTaskTimer(PummelPartyMain.getPlugin(), 0, 20);
    }

    private void forceThrowAll() {
        sendBoardMessage(null, player -> {
            PlayerCharacter character = PummelPartyMain.getCharacterStorage().getCharacter(player);
            if (character.getDiceThrow() == 0) character.throwDice();
        });
        startRound();
    }

    private void startRound() {
        players.values().stream()
                .sorted((e1, e2) -> e2.getDiceThrow() - e1.getDiceThrow())
                .forEach(queue::add);
        gameTimer = 60;
        PlayerCharacter playerTurn = queue.peek();
        sendBoardMessage("It's " + playerTurn.getPlayer() + "'s turn!");
        //todo make the player roll the dice
        new BukkitRunnable() {
            //update stuff every second
            @Override
            public void run() {
                //start the game if the timer is on 0
                if (gameTimer == 0) {
                    this.cancel();
                }
                gameTimer--;
            }
        }.runTaskTimer(PummelPartyMain.getPlugin(), 0, 20);
    }


    private boolean checkIfAllThrown() {
        for (PlayerCharacter value : players.values()) {
            if (value.getDiceThrow() == 0) {
                return false;
            }
        }
        return true;
    }

    private boolean lobbyStarted = false;

    private void startLobbyTimer() {
        state = GameState.STARTING;
        lobbyStarted = true;
        //we reset the scoreboard so it can show the timeleft
        players.keySet().forEach(u -> {
            PummelPartyMain.getScoreboardManager()
                    .setScoreboard(u, ScoreboardType.WAITING_LOBBY);
        });
        new BukkitRunnable() {
            //update stuff every second
            @Override
            public void run() {
                //start the game if the timer is on 0
                if (gameTimer == 0) {
                    this.cancel();
                    startGame();
                }
                //check if the players are at max and reduce the timer to 10
                if (players.size() == maxPlayers &&
                        gameTimer > 10) gameTimer = 10;
                //check if there are enough players
                if (players.size() < minPlayers) {
                    resetLobby();
                    this.cancel();
                }
                gameTimer--;
            }
        }.runTaskTimer(PummelPartyMain.getPlugin(), 0, 20);
    }

    private void generate(Block spawnBlock) {
        this.spawn = new Pad(spawnBlock);
        //make this block as the main spawn block
        pads.add(this.spawn);
        //try & find surrounding blocks
        findSurrounding(this.spawn);
    }

    public void removeSpawn() {
        pads.remove(this.spawn);
        pads.clear();
        this.spawn = null;

    }

    private boolean generateGraveyard(Block graveyard) {
        Pad pad = new Pad(graveyard);
        for (Pad gv : graveyards) {
            if (gv.getLocation() == pad.getLocation())
                return false;
        }
        graveyards.add(pad);
        findSurrounding(pad);
        return true;
    }

    //made some changes here (namely i try getting Padtype from block & Pad & compare them instead
    private boolean checkBlock(Block block, Pad currentPad) throws BlockNotPadException {
        //check if currentpad can move to block's padtype
        Padtype newPadType = Padtype.getFromBlock(block);
        if (newPadType == Padtype.NULL) {
            throw new BlockNotPadException();
        }

        if (newPadType.canMoveFrom(currentPad.getPadtype())) {
            //check if the current pad is a child of the next pad
            Pad newPad = getByLocation(block.getLocation());
            if (newPad != null) {
                for (Pad child : newPad.getChildren()) {
                    if (child.getLocation().equals(currentPad.getLocation())) return false;
                }
                for (Pad parent : newPad.getParents()) {
                    if (parent.getLocation().equals(currentPad.getLocation())) return false;
                }
            } else {
                newPad = new Pad(block);
                pads.add(newPad);
            }

            currentPad.getChildren().add(newPad);
            newPad.getParents().add(currentPad);

            findSurrounding(newPad);
            return true;
        }
        return false;
    }

    //get a pad from the list of existing pads by location
    private Pad getByLocation(Location location) {
        for (Pad pad : pads) {
            if (pad.getLocation().equals(location)) return pad;
        }
        return null;
    }

    //best to clone locations so original isn't modified (sometimes bugs)
    private void findSurrounding(Pad pad) {
        Block[] blocks = {
                pad.getLocation().getWorld().getBlockAt(pad.getLocation().clone().add(1, 0, 0)),
                pad.getLocation().getWorld().getBlockAt(pad.getLocation().clone().add(-1, 0, 0)),
                pad.getLocation().getWorld().getBlockAt(pad.getLocation().clone().add(0, 0, 1)),
                pad.getLocation().getWorld().getBlockAt(pad.getLocation().clone().add(0, 0, -1))
        };
        int add = 0;
        for (int i = 0; i < 4; i++) {
            //checking if any adjacent block is any kind of path
            try {
                if (checkBlock(blocks[i], pad)) {
                    add = 0;
                    continue;
                }
                //no need to check up and down if checkBlock is already a pad
            } catch (BlockNotPadException ignored) {
            }

            //We check the up/down blocks cuz terrain
            if (add == 0) {
                blocks[i] = blocks[i].getWorld().getBlockAt(blocks[i].getLocation().clone().add(0, -1, 0));
                add++;
            } else if (add == 1) {
                blocks[i] = blocks[i].getWorld().getBlockAt(blocks[i].getLocation().clone().add(0, 2, 0));
                add++;
            } else {
                add = 0;
                continue;
            }
            i--;
        }

    }

    public Pad getSpawn() {
        return spawn;
    }

    public void setSpawn(Block spawn) {
        generate(spawn);
    }

    public void setSpawn(Pad spawn) {
        this.spawn = spawn;
    }

    public List<Pad> getPads() {
        return pads;
    }

    public void setPads(List<Pad> pads) {
        this.pads = pads;
    }

    public UUID[] getPlayers() {
        return players.keySet().toArray(new UUID[0]);
    }

    public Collection<PlayerCharacter> getPlayersCharacters() {
        return players.values();
    }

    public Map<UUID, PlayerCharacter> getPlayersMap() {
        return players;
    }


    public List<Pad> getGraveyards() {
        return graveyards;
    }

    public boolean addGraveyard(Block graveyard) {
        return generateGraveyard(graveyard);
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public String getMapName() {
        return mapName;
    }

    public int getPlayTime() {
        return playTime;
    }

    public void setPlayTime(int time) {
        this.playTime = time;
    }

    public Cuboid getCuboid() {
        return this.cuboid;
    }

    public int getPlayerRoundTime() {
        return playerRoundTime;
    }

    public void setPlayerRoundTime(int playerRoundTime) {
        this.playerRoundTime = playerRoundTime;
    }

    public int getLobbyWaitTimer() {
        return lobbyWaitTimer;
    }

    public int getGameTimer() {
        return gameTimer;
    }

    public void setLobbyWaitTimer(int lobbyWaitTimer) {
        this.lobbyWaitTimer = lobbyWaitTimer;
    }

    public int getMaxRounds() {
        return maxRounds;
    }

    public void setMaxRounds(int maxRounds) {
        this.maxRounds = maxRounds;
    }

    public Location getLobbySpawn() {
        return lobbySpawn;
    }

    public void setLobbySpawn(Location lobbySpawn) {
        this.lobbySpawn = lobbySpawn;
    }

    public GameState getState() {
        return state;
    }

    //send a message to everyone in the board
    //also executes functions
    @SafeVarargs
    public final void sendBoardMessage(String message, Consumer<Player>... func) {
        for (UUID uuid : players.keySet()) {
            Player player = Bukkit.getPlayer(uuid);
            if (player != null) {
                if (message != null) player.sendMessage(message);
                for (Consumer<Player> function : func) {
                    function.accept(player);
                }
            }
        }
    }

    //set a color to all players
    public void setupPlayersColors() {
        ChatColor[] colors = {ChatColor.RED, ChatColor.DARK_AQUA, ChatColor.GREEN, ChatColor.YELLOW,
                ChatColor.LIGHT_PURPLE, ChatColor.GOLD, ChatColor.AQUA, ChatColor.DARK_PURPLE};
        int i = 0;
        for (PlayerCharacter character : players.values()) {
            character.setGameColor(colors[i++]);
        }
    }

    public Queue<PlayerCharacter> getQueue() {
        return queue;
    }
}
