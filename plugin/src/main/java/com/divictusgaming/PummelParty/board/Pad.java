package com.divictusgaming.PummelParty.board;

import org.bukkit.Location;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.List;

public class Pad {
    private Location location;
    private Padtype type;
    private List<Pad> parents;
    private List<Pad> children;

    public Pad() {
        this.parents = new ArrayList<>();
        this.children = new ArrayList<>();
    }

    public Pad(Block block) {
        this(block.getLocation(), Padtype.getFromBlock(block));
    }


    public Pad(Location location, Padtype type) {
        this();
        this.location = location;
        this.type = type;
    }

    public List<Pad> getParents() {
        return parents;
    }

    public void setParents(List<Pad> parents) {
        this.parents = parents;
    }

    public List<Pad> getChildren() {
        return children;
    }

    public void setChildren(List<Pad> children) {
        this.children = children;
    }

    public Location getLocation() {
        return location;
    }

    public Padtype getPadtype() {
        return type;
    }

    public boolean hasPath(Block block) {
        for (Pad child : this.getChildren()) {
            if (child.getLocation().equals(block.getLocation())) {
                return true;
            }
        }
        return false;
    }

    //function to fetch next Child fields
    public List<Location> getChildrenFieldLocations() {
        List<Location> locations = new ArrayList<>();
        //logic loop through all children Pads
        Pad currentPad = this;
        for (Pad nextPad : children) {
            locations.add(getLastField(currentPad, nextPad));
        }
        return locations;
    }

    private Location getLastField(Pad currentPad, Pad nextPad) {
        //check if moving is possible
        if (nextPad.getPadtype().canMoveFrom(currentPad.getPadtype()) && currentPad.getPadtype().canMoveTo(nextPad.getPadtype())) {
            //get nextPads children
            List<Pad> nextPadsChildren = nextPad.getChildren();
            //if children>1 then its is a node if not then repeat this (using recursion)
            if (nextPadsChildren.size() == 1)
                return getLastField(nextPad, nextPadsChildren.get(0));
            return nextPad.getLocation();
        }
        return currentPad.getLocation();
    }
}
