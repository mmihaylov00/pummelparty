package com.divictusgaming.PummelParty.board;


import org.bukkit.Color;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.divictusgaming.PummelParty.util.CommonUtils;
import com.divictusgaming.PummelParty.util.UMaterials;


public enum Padtype {
    //added color to them as well (i use it in boardWizard so we can see which pads are recognised)
    //old enum format <name>(umaterial,Padtypes can move from)
    //new format <name>(umaterial,color,Padtypes can move from)
    AVAILABLE_PATH("LIME_CARPET", "lime"),
    CLOSED_PATH("RED_CARPET", "red", Padtype.AVAILABLE_PATH),

    //can't step on CLOSED_PATH
    FIELD("YELLOW_CARPET", "yellow", Padtype.CLOSED_PATH),
    SPAWN_FIELD("WHITE_CARPET", "white", PadEffect.SPAWN_FIELD, Padtype.CLOSED_PATH),
    HEAL_FIELD("MAGENTA_CARPET", "magenta", PadEffect.HEAL_FIELD, Padtype.CLOSED_PATH),
    MYSTERY_BOX_FIELD("ORANGE_CARPET", "orange", PadEffect.MYSTERY_BOX_FIELD, Padtype.CLOSED_PATH),
    DAMAGE_FIELD("BLACK_CARPET", "black", PadEffect.DAMAGE_FIELD, Padtype.CLOSED_PATH),
    REAPER_FIELD("BROWN_CARPET", "brown", PadEffect.REAPER_FIELD, Padtype.CLOSED_PATH),
    GRAVEYARD("GRAY_CARPET", "gray", PadEffect.GRAVEYARD_FIELD, Padtype.CLOSED_PATH),

    NULL("BLACK_CARPET", "black", false);

    private final UMaterials type;
    private final Color color;
    private final boolean moveall;
    private Padtype[] moveable;
    private final PadEffect effect;

    Padtype(String type, String color) {
        this.type = UMaterials.valueOf(type);
        this.effect = null;
        this.color = CommonUtils.getColorFromName(color);
        moveall = true;
    }

    Padtype(String type, String color, boolean bool) {
        this.type = UMaterials.valueOf(type);
        this.color = CommonUtils.getColorFromName(color);
        this.effect = null;
        moveall = bool;
    }

    Padtype(String type, String color, Padtype... movable) {
        this.type = UMaterials.valueOf(type);
        this.moveable = movable;
        this.color = CommonUtils.getColorFromName(color);
        this.effect = null;
        moveall = false;
    }

    Padtype(String type, String color, PadEffect effect, Padtype... moveable) {
        this.type = UMaterials.valueOf(type);
        this.color = CommonUtils.getColorFromName(color);
        this.moveable = moveable;
        this.effect = effect;
        moveall = false;
    }

    public static Padtype getFromBlock(Block block) {
        UMaterials mat = UMaterials.getItem(block);
        for (Padtype pads : Padtype.values()) {
            //check from effect first
            if (pads.effect != null) {
                if (pads.effect.type.equals(mat))
                    return pads;
            }
            //check from non effect
            if (pads.type.equals(mat))
                return pads;
        }
        return Padtype.NULL;
    }

    public Color getColor() {
        return color;
    }

    public PadEffect getEffect() {
        return effect;
    }

    public boolean canMoveFrom(Padtype pad) {
        if (moveall)
            return true;
        if (moveable != null)
            for (Padtype type : moveable)
                if (type.equals(pad))
                    return true;
        return false;
    }

    public boolean canMoveTo(Padtype pad) {
        //can false only if effect isn't null
        if (effect == null)
            return true;
        return effect.canMoveTo(pad);
    }

    public UMaterials getUmaterial() {
        //if effect then change
        if (effect != null)
            return effect.getUmaterial();
        return this.type;
    }

    public ItemStack getItemStack() {
        if (effect != null)
            return effect.getItemStack();
        return this.type.getItemStack();
    }

    public void applyEffect(Player player) {
        if (effect != null)
            effect.applyEffect(player);
    }

    public enum PadEffect {
        //enum format <name>(umaterial,effectString,umaterials can move to)
        SPAWN_FIELD(UMaterials.WHITE_CARPET, "spawnpoint", UMaterials.LIME_CARPET),
        GRAVEYARD_FIELD(UMaterials.GRAY_CARPET, "graveyard", UMaterials.LIME_CARPET),
        HEAL_FIELD(UMaterials.MAGENTA_CARPET, "heal", UMaterials.LIME_CARPET),
        MYSTERY_BOX_FIELD(UMaterials.ORANGE_CARPET, "mysterybox", UMaterials.LIME_CARPET),
        DAMAGE_FIELD(UMaterials.BLACK_CARPET, "damage", UMaterials.LIME_CARPET),
        REAPER_FIELD(UMaterials.BROWN_CARPET, "reaper", UMaterials.LIME_CARPET);

        private final UMaterials type;
        private final String effect;
        private final UMaterials[] moveto;

        PadEffect(UMaterials type, String effect, UMaterials... moveto) {
            this.type = type;
            this.effect = effect;
            this.moveto = moveto;
        }

        private boolean canMoveTo(Padtype type) {
            if (moveto != null)
                for (UMaterials mat : moveto)
                    if (type.getUmaterial().equals(mat))
                        return true;
            return false;
        }

        private UMaterials getUmaterial() {
            return type;
        }

        private ItemStack getItemStack() {
            return type.getItemStack();
        }

        private void applyEffect(Player player) {
            //TODO make logics for each effect based on string effect
        }
    }
}
