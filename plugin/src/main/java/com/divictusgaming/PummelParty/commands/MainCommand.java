package com.divictusgaming.PummelParty.commands;

import com.divictusgaming.PummelParty.PummelPartyMain;
import com.divictusgaming.PummelParty.util.Dice;
import com.divictusgaming.PummelParty.util.JSONMessage;
import com.divictusgaming.PummelParty.board.Board;
import com.divictusgaming.PummelParty.enums.GameState;
import com.divictusgaming.PummelParty.enums.Permissions;
import com.divictusgaming.PummelParty.data.yaml.YAMLWorker;
import com.divictusgaming.PummelParty.data.yaml.dto.BoardYAMLConfiguration;
import com.divictusgaming.PummelParty.data.yaml.dto.MessagesYAMLConfiguration;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainCommand implements CommandExecutor, TabCompleter {


    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player) {
            MessagesYAMLConfiguration messages = PummelPartyMain.getMessages();
            Player player = (Player) commandSender;
            if (strings.length > 0) {
                if (strings[0].equalsIgnoreCase("dice")) {
                    //check the dice after the animation is completed (dont while loop it will crash the thread)
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            Bukkit.broadcastMessage("You rolled a: " + Dice.getLastRoll(player));
                        }
                    }.runTaskLater(PummelPartyMain.getPlugin(), Dice.getAnimationTicks());
                    return true;
                }
                if (strings[0].equalsIgnoreCase("board"))    //made sub part for board setup (mainly since we will have alot more to add to this command)
                {
                    if (!player.hasPermission(Permissions.SETUP_BOARD_WIZARD.getPermission()))    //test for permission & send no perm message
                    {
                        player.sendMessage(messages.getPrefix() + messages.getNoPermission());
                        return true;
                    }
                    setupBoards(player, strings);// made separate portion for boards
                    return true;
                }
                if (strings[0].equalsIgnoreCase("setspawn")) {
                    if (!player.hasPermission(Permissions.FULL.getPermission()))    //test for permission & send no perm message
                    {
                        player.sendMessage(messages.getPrefix() + messages.getNoPermission());
                        return true;
                    }
                    PummelPartyMain.getConfiguration().setSpawnLocation(player.getLocation());
                    YAMLWorker.saveSingleField(PummelPartyMain.getConfiguration(), "spawnLocation");
                    player.sendMessage(messages.getPrefix() + messages.getSpawnSet());
                    return true;
                }
                if (strings[0].equalsIgnoreCase("join")) {
                    if (strings.length > 1) {
                        Board board = PummelPartyMain.getBoards().get(strings[1]);
                        if (board == null) {
                            player.sendMessage(messages.getPrefix() + messages.getBoardJoinNotFound());
                            return true;
                        } else {
                            board.join(player);
                            return true;
                        }
                    }
                    player.sendMessage(messages.getPrefix() + messages.getBoardJoinTypeBoardName());
                    return true;
                }
                if (strings[0].equalsIgnoreCase("leave")) {
                    Board board = PummelPartyMain.getCharacterStorage().getCharacter(player).getBoard();
                    if (board == null) {
                        player.sendMessage(messages.getPrefix() + messages.getBoardLeaveNotInGame());
                        return true;
                    }
                    board.leave(player);
                    return true;
                }
                if (strings[0].equalsIgnoreCase("reload"))    //made sub part for board setup (mainly since we will have alot more to add to this command)
                {
                    if (!player.hasPermission(Permissions.FULL.getPermission()))    //test for permission & send no perm message
                    {
                        player.sendMessage(messages.getPrefix() + messages.getNoPermission());
                        return true;
                    }
                    player.sendMessage(messages.getPrefix() + messages.getReloadStarted());
                    Bukkit.getConsoleSender().sendMessage(messages.getPrefix() + messages.getReloadStarted());
                    PummelPartyMain.reload();
                    player.sendMessage(messages.getPrefix() + messages.getReloadEnded());
                    Bukkit.getConsoleSender().sendMessage(messages.getPrefix() + messages.getReloadEnded());
                    return true;
                }
            }
            //TODO send help message to player (add it at end)
            player.sendMessage(messages.getPrefix() + messages.getHelpUsage());
            if (player.hasPermission(Permissions.FULL.getPermission())) {
                player.sendMessage(messages.getHelpReload());
                player.sendMessage(messages.getHelpBoard());
                player.sendMessage(messages.getHelpSetSpawn());
            } else if (player.hasPermission(Permissions.SETUP_BOARD_WIZARD.getPermission())) {
                player.sendMessage(messages.getHelpBoard());
            }
            player.sendMessage(messages.getHelpJoin());
            player.sendMessage(messages.getHelpLeave());
        } else {

            //todo console commands
        }
        return true;

    }

    private void setupBoards(Player player, String[] sub) {
        MessagesYAMLConfiguration messages = PummelPartyMain.getMessages();
        if (sub.length > 1) {
            if (sub[1].equalsIgnoreCase("list"))//list all boards
            {
                int count = 1;
                HashMap<String, Board> boards = PummelPartyMain.getBoards();
                player.sendMessage(messages.getBoardPrefix() + messages.getListSavedBoards());
                for (String boardName : boards.keySet()) {
                    JSONMessage.create(messages.getListBoard(count + "", boards.get(boardName).getMapName(), boards.get(boardName).getState().getText()))
                            .tooltip(messages.getEditTooltip()).runCommand("/pp board edit " + boardName).send(player);
                    count++;
                }
                return;
            }
            if (sub[1].equalsIgnoreCase("create")) //try & create a new board
            {
                if (sub.length <= 2) {
                    player.sendMessage(messages.getBoardPrefix() + messages.getCreateNoName());
                    player.sendMessage(messages.getCreateCommand());
                    return;
                }
                //get board name (can have spaces but its better if we replace them with _)
                StringBuilder boardName = new StringBuilder(sub[2]);
                for (int i = 3; i < sub.length; i++)
                    boardName.append("_").append(sub[i]);
                //check if any board by that name exist
                if (PummelPartyMain.getBoards().containsKey(boardName.toString())) {
                    player.sendMessage(messages.getBoardPrefix() + messages.getCreateAlreadyExists());
                    return;
                }
                //try & start boardWizzard
                if (PummelPartyMain.getBoardWizzard().startSetup(player, boardName.toString())) {
                    JSONMessage.create(messages.getBoardPrefix() + messages.getWizardSetWaitingLobby())
                            .tooltip(messages.getWizardSetWaitingLobbyTooltip())
                            .runCommand("/pp board setlobbyspawn").send(player);
                    return;
                } else {
                    player.sendMessage(messages.getBoardPrefix() + messages.getCreateAlreadyInWizard());
                    return;
                }
            }
            if (sub[1].equalsIgnoreCase("edit")) {
                if (sub.length <= 2) {
                    player.sendMessage(messages.getBoardPrefix() + messages.getEditNoName());
                    player.sendMessage(messages.getEditCommand());
                    return;
                }
                //get board name (can have spaces but its better if we replace them with _)
                StringBuilder boardName = new StringBuilder(sub[2]);
                for (int i = 3; i < sub.length; i++)
                    boardName.append("_").append(sub[i]);
                //check if any board by that name exist
                if (!PummelPartyMain.getBoards().containsKey(boardName.toString())) {
                    player.sendMessage(messages.getBoardPrefix() + messages.getEditBoardNotFound());
                    return;
                }
                Board board = PummelPartyMain.getBoards().get(boardName.toString());

                player.sendMessage(messages.getBoardPrefix() + messages.getEditEditing(boardName.toString()));
                PummelPartyMain.getBoardWizzard().openEditor(player, board);
                return;
            }
            if (sub[1].equalsIgnoreCase("delete")) {
                if (sub.length <= 2) {
                    player.sendMessage(messages.getBoardPrefix() + messages.getDeleteNoName());
                    player.sendMessage(messages.getDeleteCommand());
                    return;
                }
                //get board name (can have spaces but its better if we replace them with _)
                StringBuilder boardName = new StringBuilder(sub[2]);
                for (int i = 3; i < sub.length; i++)
                    boardName.append("_").append(sub[i]);
                //check if any board by that name exist
                if (!PummelPartyMain.getBoards().containsKey(boardName.toString())) {
                    player.sendMessage(messages.getBoardPrefix() + messages.getDeleteBoardNotFound());
                    return;
                }
                //delete this board
                //remove this from all saved boards
                PummelPartyMain.getBoards().remove(boardName.toString());
                //delete the file as well
                HashMap<String, String> placeholders = new HashMap<>();
                placeholders.put("name", boardName.toString());
                YAMLWorker.loadFile(new BoardYAMLConfiguration(), placeholders).delete();

                player.sendMessage(messages.getBoardPrefix() + messages.getDeleteSuccess(boardName.toString()));
                return;
            }
            if (sub[1].equals("select-corner"))//used in boardwizzard
            {
                PummelPartyMain.getBoardWizzard().setBravoLocation(player);
                return;
            }
            if (sub[1].equals("start-corner"))//used in boardwizzard
            {
                PummelPartyMain.getBoardWizzard().setAlphaLocations(player);
                return;
            }
            if (sub[1].equals("finish"))//used in boardwizzard
            {
                if (!PummelPartyMain.getBoardWizzard().checkFinished(player)) {
                    player.sendMessage(messages.getBoardPrefix() + messages.getBoardWizardFailed());
                } else {
                    String boardName = PummelPartyMain.getBoardWizzard().getBoardName(player);
                    player.sendMessage(messages.getBoardPrefix() + messages.getBoardWizardSuccess(boardName));
                    JSONMessage.create(messages.getBoardPrefix() + messages.getBoardWizardEditSettings())
                            .tooltip(messages.getBoardWizardEditSettingsTooltip())
                            .runCommand("/pp board edit " + boardName).send(player);
                }
                PummelPartyMain.getBoardWizzard().cancelCreation(player);
                return;
            }
            if (sub[1].equals("setlobbyspawn"))//used in boardwizzard
            {
                PummelPartyMain.getBoardWizzard().setWaitingLobby(player);
                return;
            }
        }
        //send help
        JSONMessage.create(messages.getBoardPrefix() + messages.getBoardHelpUsage()).send(player);
        JSONMessage.create(messages.getBoardHelpCreate()).tooltip(messages.getBoardHelpCreateTooltip())
                .suggestCommand("/pp board create ").send(player);
        JSONMessage.create(messages.getBoardHelpDelete()).tooltip(messages.getBoardHelpDeleteToolTip())
                .suggestCommand("/pp board delete ").send(player);
        JSONMessage.create(messages.getBoardHelpEdit()).tooltip(messages.getBoardHelpEditTooltip())
                .suggestCommand("/pp board edit ").send(player);
        JSONMessage.create(messages.getBoardHelpList()).tooltip(messages.getBoardHelpListTooltip())
                .runCommand("/pp board list").send(player);
        JSONMessage.create(messages.getBoardHelpFinish()).send(player);
    }

    //register auto tab completer
    public List<String> onTabComplete(CommandSender sender,
                                      Command command,
                                      String alias,
                                      String[] args) {
        if (command.getName().equalsIgnoreCase("pp") ||
                command.getName().equalsIgnoreCase("pummelparty")) {
            List<String> l = new ArrayList<>();
            if (args.length == 1) {
                if (sender.hasPermission(Permissions.FULL.getPermission())) {
                    l.add("board");
                    l.add("reload");
                    l.add("setspawn");
                } else if (sender.hasPermission(Permissions.SETUP_BOARD_WIZARD.getPermission())) {
                    l.add("board");
                }
                l.add("join");
                l.add("leave");
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("board") &&
                        (sender.hasPermission(Permissions.SETUP_BOARD_WIZARD.getPermission()) ||
                                sender.hasPermission(Permissions.FULL.getPermission()))) {
                    l.add("create");
                    l.add("delete");
                    l.add("edit");
                    l.add("list");
                    l.add("finish");
                } else if (args[0].equalsIgnoreCase("join")) {
                    for (Map.Entry<String, Board> entry : PummelPartyMain.getBoards().entrySet()) {
                        if (entry.getValue().getState() == GameState.AVAILABLE || entry.getValue().getState() == GameState.STARTING) {
                            l.add(entry.getKey());
                        }
                    }
                }
            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("board") &&
                        (sender.hasPermission(Permissions.SETUP_BOARD_WIZARD.getPermission()) ||
                                sender.hasPermission(Permissions.FULL.getPermission()))) {
                    if (args[1].equalsIgnoreCase("delete")) {
                        l.addAll(PummelPartyMain.getBoards().keySet());
                    } else if (args[1].equalsIgnoreCase("edit")) {
                        l.addAll(PummelPartyMain.getBoards().keySet());
                    }
                }
            }
            String arg = args[args.length - 1].trim();
            if (!arg.isEmpty()) {
                for (int i = 0; i < l.size(); i++) {
                    String s = l.get(i);
                    if (!s.startsWith(arg)) {
                        l.remove(i--);
                    }
                }
            }
            return l; //returns the possibility's to the client


        }
        return null;
    }
}
