package com.divictusgaming.PummelParty.data.yaml;

import com.divictusgaming.PummelParty.PummelPartyMain;
import com.divictusgaming.PummelParty.util.StringFormater;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLFileLocation;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLFileName;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLObject;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLPath;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLPathPrefix;
import com.divictusgaming.PummelParty.data.yaml.dto.YAMLConfiguration;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class YAMLWorker {
    private static final File PATH = PummelPartyMain.getPluginsFolder();

    public static <T extends YAMLConfiguration> void readFile(T configuration) {
        readFile(configuration, null);
    }

    public static <T extends YAMLConfiguration> void readFile(T configuration, Map<String, String> placeholders) {
        File configFile = loadFile(configuration, placeholders);
        //check if the file is saved or not
        if (!configFile.exists())
            throw new NullPointerException("File not found");
        //load that file
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(configFile);//attempt to load the file
        //get prefix
        String prefix = loadPrefix(configuration, placeholders);
        //start reading each field in the class & setting them
        readField(fileConfiguration, configuration, prefix);
    }

    public static <T extends YAMLConfiguration> String getFileLocation(T configuration) {
        String fileLocation = "";
        if (configuration.getClass().isAnnotationPresent(YAMLFileLocation.class))//load file in a folder
        {
            fileLocation = configuration.getClass().getAnnotation(YAMLFileLocation.class).location(); //fetch location string
            //check if it begins with /
            if (!fileLocation.startsWith("/"))
                fileLocation = "/" + fileLocation;

        }
        return fileLocation;
    }

    public static <T extends YAMLConfiguration> String getFileName(T configuration) {
        if (!configuration.getClass().isAnnotationPresent(YAMLFileName.class)) //
        {
            throw new RuntimeException("Missing @YAMLFileName annotation on the " + configuration.getClass().getSimpleName() + " class!");
        }
        String fileName = configuration.getClass().getAnnotation(YAMLFileName.class).name();
        if (!fileName.matches("^[A-Za-z0-9_-]+\\.yml$")) {
            if (fileName.endsWith(".")) fileName += "yml";
            else fileName += ".yml";
        }
        return fileName;
    }

    public static <T extends YAMLConfiguration> void saveFile(T configuration) {
        saveFile(configuration, null);
    }

    //adds the missing fields to the configuration
    public static <T extends YAMLConfiguration> void addMissing(T configuration) {
        File configFile = loadFile(configuration); //load file from the class itself
        String prefix = loadPrefix(configuration, null); //load path prefix from class
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(configFile); //attempt loading YAMLconfig from file

        saveFields(fileConfiguration, configuration, prefix, false); //save all fields declared in class to file
        try {
            fileConfiguration.save(configFile);
        } catch (IOException e) {
        }
    }

    public static <T extends YAMLConfiguration> void saveFile(T configuration, Map<String, String> placeholders) //modified some things (added YAMLFileLocation annotaion so sorting files is easier)
    {
        File configFile = loadFile(configuration, placeholders); //load file from the class itself
        String prefix = loadPrefix(configuration, placeholders); //load path prefix from class
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(configFile); //attempt loading YAMLconfig from file

        saveFields(fileConfiguration, configuration, prefix, true); //save all fields declared in class to file
        //save the file itself on machine
        try {
            fileConfiguration.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //save a single field to config, currently used for the setspawn command. Use only for basic types/enums
    public static <T extends YAMLConfiguration> void saveSingleField(T configuration, String fieldName) //modified some things (added YAMLFileLocation annotaion so sorting files is easier)
    {
        File configFile = loadFile(configuration, null);
        String prefix = loadPrefix(configuration, null);
        FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(configFile); //attempt loading YAMLconfig from file
        for (Field declaredField : configuration.getClass().getDeclaredFields()) {
            if (declaredField.getName().equalsIgnoreCase(fieldName)) {
                declaredField.setAccessible(true); //change it to accessable
                if (declaredField.isAnnotationPresent(YAMLPath.class))  //check if field had path annotation
                {
                    String path = declaredField.getAnnotation(YAMLPath.class).path(); //get the path
                    try {
                        if (declaredField.get(configuration) == null) {
                            break;
                        }
                        if (declaredField.getType() == Location.class) {
                            Object o = declaredField.get(configuration);
                            fileConfiguration.set(prefix + path, StringFormater.locationToString((Location) o, true));
                        } else if (declaredField.getType() == String.class ||    //test if field is a store able type(can be set as is)
                                declaredField.getType() == Double.class ||
                                declaredField.getType() == Integer.class ||
                                declaredField.getType() == Boolean.class) {
                            Object o = declaredField.get(configuration);
                            fileConfiguration.set(prefix + path, o);
                        } else if (declaredField.getType().isEnum())    //test if field is an enum type
                        {
                            Enum o = (Enum) declaredField.get(configuration);
                            if (o != null) fileConfiguration.set(prefix + path, o.name());    //save only its name
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                declaredField.setAccessible(false); //reset field to inaccessable
                break;
            }
        }
        //save the file itself on machine
        try {
            fileConfiguration.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private static YAMLConfiguration readField(FileConfiguration fileConfiguration, YAMLConfiguration configuration, String prefix) {
        for (Field declaredField : configuration.getClass().getDeclaredFields()) //fetch all declared fields
        {
            declaredField.setAccessible(true); //change them to accessable
            if (declaredField.isAnnotationPresent(YAMLPath.class))  //check if field had path annotation
            {
                String path = declaredField.getAnnotation(YAMLPath.class).path(); //get the path
                //check if path is set
                if (!fileConfiguration.isSet(prefix + path))
                    continue;
                try {
                    if (declaredField.getType() == Location.class) {
                        Object o = fileConfiguration.get(prefix + path);
                        declaredField.set(configuration, StringFormater.stringToLocation((String) o));
                    } else if (fileConfiguration.get(prefix + path).getClass().equals(declaredField.getType())) //check if saved object is same as field type
                    {
                        Object o = fileConfiguration.get(prefix + path);//get the object
                        declaredField.set(configuration, o); //set this in configuration
                    } else if (declaredField.getType().isEnum())    //test if field is an enum type
                    {
                        //get enum name from file
                        String enumName = fileConfiguration.getString(prefix + path);
                        //get enum object
                        Class<? extends Enum> o = (Class<? extends Enum>) declaredField.getType();
                        //set enum in declared fields
                        declaredField.set(configuration, Enum.valueOf(o, enumName));
                    } else if (List.class.isAssignableFrom(declaredField.getType()))  //check if its a list of objects
                    {
                        if (declaredField.isAnnotationPresent(YAMLObject.class)) //if field has object annotation (means the list extends YAMLConfiguration)
                        {
                            //get class name (cant fetch from list cause runtimeExceptoin)
                            String className = declaredField.getAnnotation(YAMLObject.class).className();
                            //get length of list
                            int length = (fileConfiguration.getConfigurationSection(prefix + path).getKeys(false).size());
                            //create a new list
                            List<YAMLConfiguration> list = new ArrayList<>();
                            for (int i = 0; i < length; i++) {
                                //create an instance of the class
                                try {
                                    Class<?> c = Class.forName(className);
                                    list.add(readField(fileConfiguration, (YAMLConfiguration) c.newInstance(), prefix + path + "." + i + "."));
                                } catch (ClassNotFoundException | InstantiationException e) {
                                    e.printStackTrace();
                                }
                            }
                            //set this list to field
                            declaredField.set(configuration, list);
                        } else //if list doesn't have annotation try reading it by default method (could be better if we checked if list type is save able or not)
                        {
                            declaredField.set(configuration, fileConfiguration.get(prefix + path));
                        }
                    } else if (declaredField.isAnnotationPresent(YAMLObject.class)) {
                        //get class name (cant fetch from list cause runtimeExceptoin)
                        String className = declaredField.getAnnotation(YAMLObject.class).className();
                        try {
                            Class<?> c = Class.forName(className);
                            declaredField.set(configuration, readField(fileConfiguration, (YAMLConfiguration) c.newInstance(), prefix + path));
                        } catch (ClassNotFoundException | InstantiationException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            declaredField.setAccessible(false); //reset fields to in accessable
        }
        return configuration;
    }

    private static void saveFields(FileConfiguration fileConfiguration, YAMLConfiguration configuration, String prefix, boolean replaceExisting) {
        for (Field declaredField : configuration.getClass().getDeclaredFields()) //fetch all declared fields
        {
            declaredField.setAccessible(true); //change them to accessable
            if (declaredField.isAnnotationPresent(YAMLPath.class))  //check if field had path annotation
            {
                String path = declaredField.getAnnotation(YAMLPath.class).path(); //get the path

                try {
                    if (declaredField.get(configuration) == null) {
                        continue;
                    }
                    if (declaredField.getType() == String.class ||    //test if field is a store able type(can be set as is)
                            declaredField.getType() == Double.class ||
                            declaredField.getType() == Integer.class ||
                            declaredField.getType() == Boolean.class) {
                        Object o = declaredField.get(configuration);
                        //do we want to set it to the value from the object if it's not set
                        //used for adding missing values from the object to the file without resetting all other values
                        if (replaceExisting || !fileConfiguration.isSet(prefix + path))
                            fileConfiguration.set(prefix + path, o);
                    }
                    //if the field is a location
                    if (declaredField.getType() == Location.class) {
                        Object o = declaredField.get(configuration);
                        if (replaceExisting || !fileConfiguration.isSet(prefix + path))
                            //we want to save it as a string
                            fileConfiguration.set(prefix + path, StringFormater.locationToString((Location) o, true));
                    } else if (declaredField.getType().isEnum())    //test if field is an enum type
                    {
                        Enum o = (Enum) declaredField.get(configuration);
                        if (replaceExisting || !fileConfiguration.isSet(prefix + path))
                            if (o != null) fileConfiguration.set(prefix + path, o.name());    //save only its name
                    } else if (List.class.isAssignableFrom(declaredField.getType()))  //check if its a list of objects
                    {
                        if (declaredField.isAnnotationPresent(YAMLObject.class)) //if field has object annotation (means the list extends YAMLConfiguration)
                        {
                            List<YAMLConfiguration> list = (List<YAMLConfiguration>) declaredField.get(configuration);//get the list
                            for (int i = 0; i < list.size(); i++) {
                                YAMLConfiguration yamlConfiguration = list.get(i);
                                saveFields(fileConfiguration, yamlConfiguration, prefix + path + "." + i + ".", replaceExisting); //save the field using recursion
                            }
                        } else //lastly if list doesnt have annotation try saving it by default method (could be better if we checked if list type is save able or not)
                        //todo check if the list type is save able
                        {
                            Object o = declaredField.get(configuration);
                            if (replaceExisting || !fileConfiguration.isSet(prefix + path))
                                if (o != null) fileConfiguration.set(prefix + path, o);
                        }
                    } else if (declaredField.isAnnotationPresent(YAMLObject.class)) {
                        saveFields(fileConfiguration, (YAMLConfiguration) declaredField.get(configuration), prefix + path, replaceExisting);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            declaredField.setAccessible(false); //reset fields to inaccessable
        }
    }

    private static File loadFile(YAMLConfiguration configuration) {
        return loadFile(configuration, null);
    }

    public static File loadFile(YAMLConfiguration configuration, Map<String, String> placeholders) {
        String fileLocation = "";
        if (configuration.getClass().isAnnotationPresent(YAMLFileLocation.class))//load file in a folder
        {
            fileLocation = configuration.getClass().getAnnotation(YAMLFileLocation.class).location(); //fetch location string
            //check if it begins with /
            if (!fileLocation.startsWith("/"))
                fileLocation = "/" + fileLocation;

        }
        if (!configuration.getClass().isAnnotationPresent(YAMLFileName.class)) //
        {
            throw new RuntimeException("Missing @YAMLFileName annotation on the " + configuration.getClass().getSimpleName() + " class!");
        }
        String fileName = configuration.getClass().getAnnotation(YAMLFileName.class).name();
        //if placeholders == null, skip
        if (placeholders != null) {
            for (Map.Entry<String, String> entry : placeholders.entrySet()) {
                fileName = fileName.replaceAll("\\{" + entry.getKey() + "}", entry.getValue());
            }
        }
        if (!fileName.matches("^[A-Za-z0-9_-]+\\.yml$")) {
            if (fileName.endsWith(".")) fileName += "yml";
            else fileName += ".yml";
        }

        return new File(PATH + fileLocation, fileName);

    }

    private static String loadPrefix(YAMLConfiguration configuration, Map<String, String> placeholders) {
        if (!configuration.getClass().isAnnotationPresent(YAMLPathPrefix.class)) {
            return "";
        }
        String prefix = configuration.getClass().getAnnotation(YAMLPathPrefix.class).path();
        if (!prefix.endsWith("."))
            prefix += ".";
        if (placeholders != null) {
            for (Map.Entry<String, String> entry : placeholders.entrySet()) {
                prefix = prefix.replaceAll("\\{" + entry.getKey() + "}", entry.getValue());
            }
        }
        return prefix;
    }
}
