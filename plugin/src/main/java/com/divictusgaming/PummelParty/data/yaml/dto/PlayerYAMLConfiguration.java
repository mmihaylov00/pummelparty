package com.divictusgaming.PummelParty.data.yaml.dto;

import com.divictusgaming.PummelParty.characters.PlayerCharacter;
import com.divictusgaming.PummelParty.data.PlayerConfiguration;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLFileLocation;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLFileName;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLPath;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLPathPrefix;

@YAMLFileLocation(location = "PlayerData")
@YAMLPathPrefix(path = "data")
@YAMLFileName(name = "{uuid}.yml")
public class PlayerYAMLConfiguration extends YAMLConfiguration implements PlayerConfiguration {
    @YAMLPath(path = "uuid")
    private String uuid;

    @YAMLPath(path = "keys")
    private Integer keys;

    @YAMLPath(path = "trophies")
    private Integer trophies;

    @YAMLPath(path = "wins")
    private Integer wins;

    @YAMLPath(path = "totalGames")
    private Integer totalGames;

    public PlayerYAMLConfiguration(PlayerCharacter playerCharacter) {
        this.uuid = playerCharacter.getPlayerUUID().toString();
        this.setKeys(playerCharacter.getTotalKeys());
        this.setTrophies(playerCharacter.getTotalTrophies());
        this.setWins(playerCharacter.getWins());
        this.setTotalGames(playerCharacter.getTotalGames());
    }

    public PlayerYAMLConfiguration() {
        this.keys = 0;
        this.trophies = 0;
        this.wins = 0;
        this.totalGames = 0;
    }

    public String getUuid() {
        return uuid;
    }

    public Integer getKeys() {
        return keys;
    }

    public void setKeys(Integer keys) {
        this.keys = keys;
    }

    public Integer getTrophies() {
        return trophies;
    }

    public void setTrophies(Integer trophies) {
        this.trophies = trophies;
    }

    public Integer getWins() {
        return wins;
    }

    public void setWins(Integer wins) {
        this.wins = wins;
    }

    public Integer getTotalGames() {
        return totalGames;
    }

    public void setTotalGames(Integer totalGames) {
        this.totalGames = totalGames;
    }

}
