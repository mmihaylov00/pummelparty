package com.divictusgaming.PummelParty.data.yaml.dto;

import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLFileName;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLPath;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLPathPrefix;
import org.bukkit.Location;

import java.util.ArrayList;

@YAMLPathPrefix(path = "config")
@YAMLFileName(name = "config.yml")
public class ConfigYAMLConfiguration extends YAMLConfiguration
{
    public ConfigYAMLConfiguration() {
        setupLobbyDefaultLines();
        setupIngameDefaultLines();
        setupWaitingLobbyDefaultLines();
    }

    @YAMLPath(path = "database-type")
    private String databaseType = "file";

    @YAMLPath(path = "mysql.url")
    private String mysqlUrl = "jdbc:mysql://localhost:3306/databasename?createDatabaseIfNotExist=true";

    @YAMLPath(path = "mysql.username")
    private String mysqlUsername = "root";

    @YAMLPath(path = "mysql.password")
    private String mysqlPassword = "pass";

    @YAMLPath(path = "spawnLocation")
    private Location spawnLocation = null;

    @YAMLPath(path = "symbol.key")
    private String keySymbol = "&e&lo&e&m╖";

    @YAMLPath(path = "symbol.trophy")
    private String trophySymbol = "&e&l♕";

    //the values here are the default ones
    @YAMLPath(path = "scoreboard.lobby.title")
    private final String sbLobbyTitle = "&f&l--&b&lPummelParty&f&l--";

    @YAMLPath(path = "scoreboard.lobby.lines")
    private final ArrayList<String> sbLobbyLines = new ArrayList<>(15);

    private void setupLobbyDefaultLines(){
        sbLobbyLines.add("&0");
        sbLobbyLines.add("&a&l➤ &eTotal Keys:");
        sbLobbyLines.add("{totalKeys} {keysSymbol}");
        sbLobbyLines.add("&1");
        sbLobbyLines.add("&a&l➤ &eTrophies:");
        sbLobbyLines.add("{totalTrophies} {trophiesSymbol}");
        sbLobbyLines.add("&2");
        sbLobbyLines.add("&a&l➤ &eGames won:");
        sbLobbyLines.add("{wins}");
        sbLobbyLines.add("&3");
        sbLobbyLines.add("&a&l➤ &eTotal games:");
        sbLobbyLines.add("{gamesPlayed}");
        sbLobbyLines.add("&e&l--------------");
    }

    @YAMLPath(path = "scoreboard.in-game.title")
    private String sbIngameTitle = "&f&l--&b&lPummelParty&f&l--";

    @YAMLPath(path = "scoreboard.in-game.team-line")
    //Example: ZeaL_BG - 12 - 20
    private String teamLine = "{playerColor}{place} {player} {keys} {trophies}";

    @YAMLPath(path = "scoreboard.in-game.you-line")
    //Shows instead of the player's name in the scoreboard, can use {player} in there
    private String you = "{player} (YOU)";

    @YAMLPath(path = "scoreboard.in-game.lines")
    private ArrayList<String> sbIngameLines = new ArrayList<>(15);

    private void setupIngameDefaultLines(){
        sbIngameLines.add("&1");
        sbIngameLines.add("&e# Player {keysSymbol}&e {trophiesSymbol}");
        sbIngameLines.add("{teamLines}");
        sbIngameLines.add("&3");
        sbIngameLines.add("&a&l➤ &eTurns Order:");
        sbIngameLines.add("{order}");
        sbIngameLines.add("&e&l--------------");
    }

    @YAMLPath(path = "scoreboard.waiting-lobby.title")
    private String sbWaitingLobbyTitle = "&f&l--&b&lPummelParty&f&l--";

    @YAMLPath(path = "scoreboard.in-game.order-symbol")
    private String orderSymbol = "█";
    @YAMLPath(path = "scoreboard.in-game.no-order")
    private String noOrder = "Determining order";

    @YAMLPath(path = "scoreboard.waiting-lobby.lines")
    private final ArrayList<String> sbWaitingLobbyLines = new ArrayList<>(15);

    @YAMLPath(path = "scoreboard.waiting-lobby.starting-in-lines")
    private final ArrayList<String> sbWaitingLobbyStartingInLines = new ArrayList<>(15);

    @YAMLPath(path = "scoreboard.waiting-lobby.not-enough-players-lines")
    private final ArrayList<String> sbWaitingLobbyNotEnoughPlayers = new ArrayList<>(15);

    private void setupWaitingLobbyDefaultLines(){
        sbWaitingLobbyLines.add("&1");
        //this line will be replaced with
        //sbWaitingLobbyStartingInLines or sbWaitingLobbyNotEnoughPlayers
        sbWaitingLobbyLines.add("{startInLines}");
        sbWaitingLobbyLines.add("&2");
        sbWaitingLobbyLines.add("&a&l➤ &ePlayers:");
        sbWaitingLobbyLines.add("{current}/{max}");
        sbWaitingLobbyLines.add("&e&l--------------");

        //if the player count >= min and the game is starting
        sbWaitingLobbyStartingInLines.add("&a&l➤ &eStarting in:");
        sbWaitingLobbyStartingInLines.add("{timer} second(s)");
        //if the player count < min
        sbWaitingLobbyNotEnoughPlayers.add("&a&l➤ &ePlayers needed:");
        sbWaitingLobbyNotEnoughPlayers.add("{playersNeeded} player(s)");
    }

    public String getDatabaseType() {
        return databaseType;
    }

    public String getMysqlUrl() {
        return mysqlUrl;
    }

    public String getMysqlUsername() {
        return mysqlUsername;
    }

    public String getMysqlPassword() {
        return mysqlPassword;
    }

    public ArrayList<String> getSbWaitingLobbyStartingInLines() {
        return sbWaitingLobbyStartingInLines;
    }

    public ArrayList<String> getSbWaitingLobbyNotEnoughPlayers() {
        return sbWaitingLobbyNotEnoughPlayers;
    }

    public String getKeySymbol() {
        return keySymbol;
    }

    public String getTrophySymbol() {
        return trophySymbol;
    }

    public String getSbLobbyTitle() {
        return sbLobbyTitle;
    }

    public ArrayList<String> getSbLobbyLines() {
        return sbLobbyLines;
    }

    public String getSbIngameTitle() {
        return sbIngameTitle;
    }

    public String getYou() {
        return you;
    }

    public ArrayList<String> getSbIngameLines() {
        return sbIngameLines;
    }

    public String getTeamLine() {
        return teamLine;
    }

    public String getSbWaitingLobbyTitle() {
        return sbWaitingLobbyTitle;
    }

    public ArrayList<String> getSbWaitingLobbyLines() {
        return sbWaitingLobbyLines;
    }

    public Location getSpawnLocation() {
        return spawnLocation;
    }

    public void setSpawnLocation(Location spawnLocation) {
        this.spawnLocation = spawnLocation;
    }

    public String getOrderSymbol() {
        return orderSymbol;
    }

    public String getNoOrder() {
        return noOrder;
    }
}
