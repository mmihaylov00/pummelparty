package com.divictusgaming.PummelParty.data;

public interface PlayerConfiguration {
    String getUuid();

    Integer getKeys();

    void setKeys(Integer keys);

    Integer getTrophies();

    void setTrophies(Integer trophies);

    Integer getWins();

    void setWins(Integer wins);

    Integer getTotalGames();

    void setTotalGames(Integer totalGames);
}
