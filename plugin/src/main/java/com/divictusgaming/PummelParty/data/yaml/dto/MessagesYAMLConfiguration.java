package com.divictusgaming.PummelParty.data.yaml.dto;

import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLFileName;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLPath;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLPathPrefix;

@YAMLPathPrefix(path = "message")
@YAMLFileName(name = "messages.yml")
public class MessagesYAMLConfiguration extends YAMLConfiguration
{
    //the values here are the default ones
    @YAMLPath(path = "prefix")
    private String prefix = "&b&lPummelParty &7&l>&r ";

    //the values here are the default ones
    @YAMLPath(path = "no-permission")
    private String noPermission = "&cYou don't have permission!";

    @YAMLPath(path = "help.usage")
    private String helpUsage = "&fUsage:";

    @YAMLPath(path = "reload.started")
    private String reloadStarted = "&cReloading the plugin...";

    @YAMLPath(path = "reload.ended")
    private String reloadEnded = "&cReload completed!";

    @YAMLPath(path = "help.board")
    private String helpBoard = "&b&l >&f/pp board  &7(edit game boards)";
    @YAMLPath(path = "help.set-spawn")
    private String helpSetSpawn = "&b&l >&f/pp setspawn  &7(set the lobby spawn)";
    @YAMLPath(path = "help.reload")
    private String helpReload = "&b&l >&f/pp reload  &7(reload the configurations)";

    @YAMLPath(path = "help.join")
    private String helpJoin = "&b&l >&f/pp join <name>  &7(join a board game)";

    @YAMLPath(path = "help.leave")
    private String helpLeave = "&b&l >&f/pp leave  &7(leave a board game)";

    @YAMLPath(path = "board.prefix")
    private String boardPrefix = "&6&lBoards &7&l>&r ";

    @YAMLPath(path = "board.list.saved-boards")
    private String listSavedBoards = "&fFollowing boards are saved:";

    @YAMLPath(path = "board.list.board")
    private String listBoard = "&6{count})&f{board} - {status}";

    @YAMLPath(path = "board.edit.tooltip")
    private String editTooltip = "&7Click here to edit this Board.";

    @YAMLPath(path = "board.create.no-name")
    private String createNoName = "&fPlease enter a name for the Board.";

    @YAMLPath(path = "board.create.command")
    private String createCommand = "&6&l> &f/pp board create <name>";

    @YAMLPath(path = "board.create.name-already-exists")
    private String createAlreadyExists = "&cBoard by that name already exists!";

    @YAMLPath(path = "board.create.already-in-wizard")
    private String createAlreadyInWizard = "&cYou are already making a board. Finish it first.";

    @YAMLPath(path = "board.wizard.click-to-set-waiting-lobby")
    private String wizardSetWaitingLobby = "&fPlease stand on the waiting lobby spawn location and click here.";

    @YAMLPath(path = "board.wizard.click-to-set-waiting-lobby-tooltip")
    private String wizardSetWaitingLobbyTooltip = "&fYou can also type &n/pp board setlobbyspawn&f if you are into that.";

    @YAMLPath(path = "board.edit.no-name")
    private String editNoName = "&fPlease enter the name of the Board that you want to edit.";

    @YAMLPath(path = "board.edit.command")
    private String editCommand = "&6&l> &f/pp board edit <name>";

    @YAMLPath(path = "board.edit.board-not-found")
    private String editBoardNotFound = "&cCan't find board by that name!";

    @YAMLPath(path = "board.edit.editing")
    private String editEditing = "&fEditing board &a{board}!";

    @YAMLPath(path = "board.delete.noName")
    private String deleteNoName = "&fPlease enter the name of the Board that you want to delete.";

    @YAMLPath(path = "board.delete.command")
    private String deleteCommand = "&6&l> &f/pp board delete <name>";

    @YAMLPath(path = "board.delete.board-not-found")
    private String deleteBoardNotFound = "&cCan't find board by that name!";

    @YAMLPath(path = "board.delete.success")
    private String deleteSuccess = "&fDeleted board &a{board}&f successfully.";

    @YAMLPath(path = "board.wizard.failed")
    private String boardWizardFailed = "&cFailed to create a new board!";

    @YAMLPath(path = "board.wizard.success")
    private String boardWizardSuccess = "&fBoard &c{board}&f created sucessfully!";

    @YAMLPath(path = "board.wizard.edit-settings")
    private String boardWizardEditSettings = "&f&lClick Here&f to edit it's settings.";

    @YAMLPath(path = "board.wizard.edit-settings-tooltip")
    private String boardWizardEditSettingsTooltip = "&7Click to edit the settings";

    @YAMLPath(path = "board.wizard.canceled")
    private String boardWizardCanceled = "&cYou moved to a different world. Board creation canceled.";

    @YAMLPath(path = "board.wizard.lobby-spawn-set")
    private String boardWizardLobbySpawnSet = "&aLobby spawn set.";

    @YAMLPath(path = "board.wizard.first-corner")
    private String boardWizardFirstCorner = "&fPlease left-click a block or click here to select the first corner.";

    @YAMLPath(path = "board.wizard.set-corner-tooltip")
    private String boardWizardSetCornerTooltip = "&7Click to set the corner to your current position.";

    @YAMLPath(path = "board.wizard.first-corner-set")
    private String boardWizardFirstCornerSet = "&aFirst Corner set.";

    @YAMLPath(path = "board.wizard.second-corner")
    private String boardWizardSecondCorner = "&fLeft-click another block or click here to select the second corner.";

    @YAMLPath(path = "board.wizard.second-corner-set")
    private String boardWizardSecondCornerSet = "&aSecond Corner set.";

    @YAMLPath(path = "board.wizard.set-spawn")
    private String boardWizardSetSpawn = "&fLeft-click on a {block} &fto set the Spawn location.";

    @YAMLPath(path = "board.wizard.set-graveyard")
    private String boardWizardSetGraveyard = "&fLeft-click on a {block} &fto add a Graveyards location.";

    @YAMLPath(path = "board.wizard.graveyard-added")
    private String boardWizardGraveyardAdded = "&fAdded a grave at &a{x}, {y}, {z}&f.";

    @YAMLPath(path = "board.wizard.graveyards-size")
    private String boardWizardGraveyardsSize = "&fBoard now has &a{size}&f graves.";

    @YAMLPath(path = "board.wizard.finish")
    private String boardWizardFinish = "&f&lClick Here &fwhen you are finished.";

    @YAMLPath(path = "board.wizard.finish-tooltip")
    private String boardWizardFinishTooltip = "&7Select at least 1 spawn & graveyard before that.";

    @YAMLPath(path = "board.wizard.outside-border")
    private String boardWizardOutsideBorder = "&cThat block is outside the board region!";

    @YAMLPath(path = "board.wizard.spawn-previous")
    private String boardWizardSpawnPrevious = "&cSpawn was previously set at &f{x}, {y}, {z}&f.";

    @YAMLPath(path = "board.wizard.spawn-set")
    private String boardWizardSpawnSet = "&fSpawn now set at &a{x}, {y}, {z}&f.";

    @YAMLPath(path = "board.wizard.spawn-moved")
    private String boardWizardSpawnMoved = "&fSpawn moved!";

    @YAMLPath(path = "board.wizard.grave-already-set")
    private String boardWizardGraveAlreadySet = "&cThis Graveyard has already been set!";

    @YAMLPath(path = "board.help.usage")
    private String boardHelpUsage = "&fUsage:";

    @YAMLPath(path = "board.help.create")
    private String boardHelpCreate = "&6&l >&f/pp board create <name>  &7(create a new board)";

    @YAMLPath(path = "board.help.delete")
    private String boardHelpDelete = "&6&l >&f/pp board delete <name>  &7(delete a board)";

    @YAMLPath(path = "board.help.edit")
    private String boardHelpEdit = "&6&l >&f/pp board edit <name>  &7(edit a board)";

    @YAMLPath(path = "board.help.list")
    private String boardHelpList = "&6&l >&f/pp board list  &7(list all board)";

    @YAMLPath(path = "board.help.create-tooltip")
    private String boardHelpCreateTooltip = "&7Click here to &acreate&7 a new board.";

    @YAMLPath(path = "board.help.delete-tooltip")
    private String boardHelpDeleteToolTip = "&7Click here to &cdelete&7 a board.";

    @YAMLPath(path = "board.help.edit-tooltip")
    private String boardHelpEditTooltip = "&7Click here to &eedit&7 a board.";

    @YAMLPath(path = "board.help.list-tooltip")
    private String boardHelpListTooltip = "&7Click here to list all boards.";

    @YAMLPath(path = "board.help.finish")
    private String boardHelpFinish = "&6&l >&f/pp board finish  &7(finish board creation)";

    @YAMLPath(path = "join.not-found")
    private String boardJoinNotFound = "&cBoard not found!";

    @YAMLPath(path = "join.typeBoardName")
    private String boardJoinTypeBoardName = "&cPlease type the board's name!";

    @YAMLPath(path = "join.board-is-full")
    private String boardJoinIsFull = "&cThis board is full!";

    @YAMLPath(path = "join.already-in-board")
    private String boardJoinAlreadyInBoard = "&cYou have already joined a board game!";

    @YAMLPath(path = "join.board-has-started")
    private String boardJoinAlreadyStarted = "&cThis board has already started!";

    @YAMLPath(path = "join.player-joined")
    private String boardPlayerJoined = "&e{player} &fjoined the game &3({current}/{max})&f!";

    @YAMLPath(path = "join.player-left")
    private String boardPlayerLeave = "&e{player} &fleft the game &3({current}/{max})&f!";

    @YAMLPath(path = "leave.not-in-game")
    private String boardLeaveNotInGame = "&cYou haven't joined a board!";

    @YAMLPath(path = "spawn.set")
    private String spawnSet = "&cSpawn set to your location!";

    @YAMLPath(path = "spawn.not-set")
    private String spawnNotSet = "&cSpawn not set, please contact an administrator to set it!";

    public String getPrefix() {
        return prefix;
    }

    public String getNoPermission() {
        return noPermission;
    }

    public String getHelpUsage() {
        return helpUsage;
    }

    public String getHelpBoard() {
        return helpBoard;
    }

    public String getHelpSetSpawn() {
        return helpSetSpawn;
    }

    public String getHelpReload() {
        return helpReload;
    }

    public String getHelpJoin() {
        return helpJoin;
    }

    public String getHelpLeave() {
        return helpLeave;
    }

    public String getBoardPrefix() {
        return boardPrefix;
    }

    public String getListBoard() {
        return listBoard;
    }

    public String getReloadStarted() {
        return reloadStarted;
    }

    public String getReloadEnded() {
        return reloadEnded;
    }

    public String getListBoard(String count, String board, String status) {
        return listBoard.replaceAll("\\{count}", count)
                .replaceAll("\\{board}", board)
                .replaceAll("\\{status}", status)
                ;
    }

    public String getListSavedBoards() {
        return listSavedBoards;
    }

    public String getEditTooltip() {
        return editTooltip;
    }

    public String getCreateNoName() {
        return createNoName;
    }

    public String getCreateCommand() {
        return createCommand;
    }

    public String getCreateAlreadyExists() {
        return createAlreadyExists;
    }

    public String getCreateAlreadyInWizard() {
        return createAlreadyInWizard;
    }

    public String getWizardSetWaitingLobby() {
        return wizardSetWaitingLobby;
    }

    public String getWizardSetWaitingLobbyTooltip() {
        return wizardSetWaitingLobbyTooltip;
    }

    public String getEditNoName() {
        return editNoName;
    }

    public String getEditCommand() {
        return editCommand;
    }

    public String getEditBoardNotFound() {
        return editBoardNotFound;
    }

    public String getEditEditing() {
        return editEditing;
    }

    public String getEditEditing(String board) {
        return editEditing.replaceAll("\\{board}", board);
    }

    public String getDeleteNoName() {
        return deleteNoName;
    }

    public String getDeleteCommand() {
        return deleteCommand;
    }

    public String getDeleteBoardNotFound() {
        return deleteBoardNotFound;
    }

    public String getDeleteSuccess() {
        return deleteSuccess;
    }

    public String getDeleteSuccess(String board) {
        return deleteSuccess.replaceAll("\\{board}", board);
    }

    public String getBoardWizardFailed() {
        return boardWizardFailed;
    }

    public String getBoardWizardSuccess() {
        return boardWizardSuccess;
    }

    public String getBoardWizardSuccess(String board) {
        return boardWizardSuccess.replaceAll("\\{board}", board);
    }

    public String getBoardPlayerJoined() {
        return boardPlayerJoined;
    }

    public String getBoardPlayerLeave() {
        return boardPlayerLeave;
    }

    public String getBoardPlayerJoined(String player, String current, String max) {
        return boardPlayerJoined.replaceAll("\\{player}", player)
                .replaceAll("\\{current}", current)
                .replaceAll("\\{max}", max);
    }
    public String getBoardPlayerLeave(String player, String current, String max) {
        return boardPlayerLeave.replaceAll("\\{player}", player)
                .replaceAll("\\{current}", current)
                .replaceAll("\\{max}", max);
    }

    public String getBoardWizardEditSettings() {
        return boardWizardEditSettings;
    }

    public String getBoardWizardEditSettingsTooltip() {
        return boardWizardEditSettingsTooltip;
    }

    public String getBoardWizardFirstCorner() {
        return boardWizardFirstCorner;
    }

    public String getBoardWizardSetCornerTooltip() {
        return boardWizardSetCornerTooltip;
    }

    public String getBoardWizardFirstCornerSet() {
        return boardWizardFirstCornerSet;
    }

    public String getBoardWizardSecondCorner() {
        return boardWizardSecondCorner;
    }

    public String getBoardWizardSecondCornerSet() {
        return boardWizardSecondCornerSet;
    }

    public String getBoardWizardSetSpawn() {
        return boardWizardSetSpawn;
    }

    public String getBoardWizardSetSpawn(String block) {
        return boardWizardSetSpawn.replaceAll("\\{block}", block);
    }

    public String getBoardWizardSetGraveyard() {
        return boardWizardSetGraveyard;
    }

    public String getBoardWizardSetGraveyard(String block) {
        return boardWizardSetGraveyard.replaceAll("\\{block}", block);
    }


    public String getBoardWizardGraveyardAdded() {
        return boardWizardGraveyardAdded;
    }

    public String getBoardWizardGraveyardAdded(String x, String y, String z) {
        return boardWizardGraveyardAdded.replaceAll("\\{x}", x).replaceAll("\\{y}", y).replaceAll("\\{z}", z);
    }

    public String getBoardWizardGraveyardsSize() {
        return boardWizardGraveyardsSize;
    }

    public String getBoardWizardGraveyardsSize(String size) {
        return boardWizardGraveyardsSize.replaceAll("\\{size}", size);
    }

    public String getBoardWizardFinish() {
        return boardWizardFinish;
    }

    public String getBoardWizardFinishTooltip() {
        return boardWizardFinishTooltip;
    }

    public String getBoardWizardOutsideBorder() {
        return boardWizardOutsideBorder;
    }

    public String getBoardWizardSpawnPrevious() {
        return boardWizardSpawnPrevious;
    }

    public String getBoardWizardSpawnPrevious(String x, String y, String z) {
        return boardWizardSpawnPrevious.replaceAll("\\{x}", x).replaceAll("\\{y}", y).replaceAll("\\{z}", z);
    }

    public String getBoardWizardSpawnSet() {
        return boardWizardSpawnSet;
    }

    public String getBoardWizardSpawnSet(String x, String y, String z) {
        return boardWizardSpawnSet.replaceAll("\\{x}", x).replaceAll("\\{y}", y).replaceAll("\\{z}", z);
    }

    public String getBoardWizardSpawnMoved() {
        return boardWizardSpawnMoved;
    }

    public String getBoardWizardGraveAlreadySet() {
        return boardWizardGraveAlreadySet;
    }

    public String getBoardHelpUsage() {
        return boardHelpUsage;
    }

    public String getBoardHelpCreate() {
        return boardHelpCreate;
    }

    public String getBoardHelpDelete() {
        return boardHelpDelete;
    }

    public String getBoardHelpEdit() {
        return boardHelpEdit;
    }

    public String getBoardHelpList() {
        return boardHelpList;
    }

    public String getBoardHelpCreateTooltip() {
        return boardHelpCreateTooltip;
    }

    public String getBoardHelpDeleteToolTip() {
        return boardHelpDeleteToolTip;
    }

    public String getBoardHelpEditTooltip() {
        return boardHelpEditTooltip;
    }

    public String getBoardHelpListTooltip() {
        return boardHelpListTooltip;
    }

    public String getBoardHelpFinish() {
        return boardHelpFinish;
    }

    public String getBoardWizardCanceled() {
        return boardWizardCanceled;
    }

    public String getBoardWizardLobbySpawnSet() {
        return boardWizardLobbySpawnSet;
    }

    public String getBoardJoinNotFound() {
        return boardJoinNotFound;
    }

    public String getBoardJoinTypeBoardName() {
        return boardJoinTypeBoardName;
    }

    public String getBoardJoinIsFull() {
        return boardJoinIsFull;
    }

    public String getBoardJoinAlreadyInBoard() {
        return boardJoinAlreadyInBoard;
    }

    public String getBoardJoinAlreadyStarted() {
        return boardJoinAlreadyStarted;
    }

    public String getBoardLeaveNotInGame() {
        return boardLeaveNotInGame;
    }

    public String getSpawnSet() {
        return spawnSet;
    }

    public String getSpawnNotSet() {
        return spawnNotSet;
    }
}
