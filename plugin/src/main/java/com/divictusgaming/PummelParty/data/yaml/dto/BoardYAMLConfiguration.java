package com.divictusgaming.PummelParty.data.yaml.dto;

import com.divictusgaming.PummelParty.cuboid.Cuboid;
import com.divictusgaming.PummelParty.board.Board;
import com.divictusgaming.PummelParty.board.Pad;
import com.divictusgaming.PummelParty.board.Padtype;
import com.divictusgaming.PummelParty.data.yaml.annotations.*;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@YAMLFileLocation(location="GameBoards")
@YAMLPathPrefix(path = "board")
@YAMLFileName(name = "{name}.yml")	//changed from board_{name}.yml so loading them is easier
public class BoardYAMLConfiguration extends YAMLConfiguration 
{

    @YAMLPath(path = "name")
    private String name;

    @YAMLPath(path = "players.min")
    private Integer minPlayers;

    @YAMLPath(path = "players.max")
    private Integer maxPlayers;

    @YAMLPath(path = "players-round-time")
    private Integer playersRoundTime;

    @YAMLPath(path = "lobby.timer")
    private Integer lobbyWaitTimer;

    @YAMLPath(path = "lobby.spawn")
    private Location lobbySpawn;

    @YAMLPath(path = "max-rounds")
    private Integer maxRounds;
    
    @YAMLPath(path = "cuboid.corner-alpha")
    private Location cornerAlpha;
    
    @YAMLPath(path = "cuboid.corner-bravo")
    private Location cornerBravo;
    
    @YAMLPath(path = "playtime")
    private Integer playTime;

    @YAMLObject(className="com.divictusgaming.PummelParty.yaml.dto.PadYAMLConfiguration")
    @YAMLPath(path = "pads.list")
    private List<PadYAMLConfiguration> pads;

    @YAMLObject(className="com.divictusgaming.PummelParty.yaml.dto.PadYAMLConfiguration")
    @YAMLPath(path = "pads.graveyards")
    private List<PadYAMLConfiguration> graveyards;

    public BoardYAMLConfiguration() 
    {
        this.pads = new LinkedList<>();
    }
    public Board getBoard()
    {
    	//get corners of cuboid
        Location cornerAlpha = this.cornerAlpha;
        Location cornerBravo = this.cornerBravo;
        Location lobbySpawn = this.lobbySpawn;
    	//create a new board
    	Board board=new Board(this.name,Cuboid.create(cornerAlpha, cornerBravo), lobbySpawn);
    	//set basic data
    	board.setMaxPlayers(this.maxPlayers);
    	board.setMinPlayers(this.minPlayers);
    	board.setPlayTime(this.playTime);
    	board.setPlayerRoundTime(this.playersRoundTime);
    	board.setMaxRounds(this.maxRounds);
    	board.setLobbyWaitTimer(this.lobbyWaitTimer);
    	//start getting pads
    	for(PadYAMLConfiguration padConfig:this.pads)
    	{
    		//add pad
            if (padConfig.getPad().getPadtype() == Padtype.SPAWN_FIELD){
                board.setSpawn(padConfig.getPad());
            }
    		board.getPads().add(padConfig.getPad());
    	}
    	for(PadYAMLConfiguration graveConfig:this.graveyards)
    	{
    		board.getGraveyards().add(graveConfig.getPad());
    	}
        List<PadYAMLConfiguration> padYAMLConfigurations = this.pads;
        for (int i = 0; i < padYAMLConfigurations.size(); i++) {
            PadYAMLConfiguration padConfig = padYAMLConfigurations.get(i);

            Pad pad = board.getPads().get(i);
            for (Integer child : padConfig.getChildren()) {
                pad.getChildren().add(board.getPads().get(child));
                board.getPads().get(child).getParents().add(pad);
            }
        }
    	return board;
    }
    public BoardYAMLConfiguration(Board board) 
    {
        this.name = board.getMapName();
        this.minPlayers = board.getMinPlayers();
        this.maxPlayers = board.getMaxPlayers();
        this.pads = new LinkedList<>();
        this.cornerAlpha=board.getCuboid().getCornerAlpha();
        this.cornerBravo=board.getCuboid().getCornerBravo();
        this.playTime=board.getPlayTime();
        this.lobbyWaitTimer=board.getLobbyWaitTimer();
        this.maxRounds = board.getMaxRounds();
        this.playersRoundTime = board.getPlayerRoundTime();
        this.lobbySpawn = board.getLobbySpawn();
        for (Pad pad : board.getPads()) 
        {
            PadYAMLConfiguration p = new PadYAMLConfiguration(pad);
            for (Pad child : pad.getChildren()) 
            {
                for (int i = 0; i < board.getPads().size(); i++) 
                {
                    if (child.getLocation().equals(board.getPads().get(i).getLocation()))
                    {
                        p.addChildren(i);
                    }
                }
            }
            this.pads.add(p);
        }

        this.graveyards = new ArrayList<>();
        for (Pad pad : board.getGraveyards()) 
        {
            PadYAMLConfiguration p = new PadYAMLConfiguration(pad);
            for (Pad child : pad.getChildren()) 
            {
                for (int i = 0; i < board.getPads().size(); i++) 
                {
                    if (child.getLocation().equals(board.getPads().get(i).getLocation())){
                        p.addChildren(i);
                    }
                }
            }
            this.graveyards.add(p);
        }
    }
    
    public String getName() {
        return name;
    }

    public Integer getMinPlayers() {
        return minPlayers;
    }

    public Integer getMaxPlayers() {
        return maxPlayers;
    }

    public List<PadYAMLConfiguration> getPads() {
        return pads;
    }

    public List<PadYAMLConfiguration> getGraveyards() {
        return graveyards;
    }

    public Integer getPlayersRoundTime() {
        return playersRoundTime;
    }

    public Integer getLobbyWaitTimer() {
        return lobbyWaitTimer;
    }

    public Integer getMaxRounds() {
        return maxRounds;
    }

    public Location getLobbySpawn() {
        return lobbySpawn;
    }

    public Location getCornerAlpha() {
        return cornerAlpha;
    }

    public Location getCornerBravo() {
        return cornerBravo;
    }

    public Integer getPlayTime() {
        return playTime;
    }
}
