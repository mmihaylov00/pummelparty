package com.divictusgaming.PummelParty.data.mysql;

import com.divictusgaming.PummelParty.characters.PlayerCharacter;
import com.divictusgaming.PummelParty.data.PlayerConfiguration;
import com.divictusgaming.PummelParty.data.mysql.annotations.MySQLColumn;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayerMySQLConfiguration implements PlayerConfiguration {
    private String uuid;

    @MySQLColumn("INT")
    private Integer keys;

    @MySQLColumn("INT")
    private Integer trophies;

    @MySQLColumn("INT")
    private Integer wins;

    @MySQLColumn("INT")
    private Integer totalGames;

    public PlayerMySQLConfiguration(PlayerCharacter playerCharacter) {
        this.uuid = playerCharacter.getPlayerUUID().toString();
        this.setKeys(playerCharacter.getTotalKeys());
        this.setTrophies(playerCharacter.getTotalTrophies());
        this.setWins(playerCharacter.getWins());
        this.setTotalGames(playerCharacter.getTotalGames());
    }

    public PlayerMySQLConfiguration(ResultSet set) {
        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                if (field.isAnnotationPresent(MySQLColumn.class)) {
                    field.setAccessible(true);
                    switch (field.getAnnotation(MySQLColumn.class).value()) {
                        case "INT": {
                            field.setInt(this, set.getInt(field.getName()));
                            break;
                        }
                        default:
                        case "VARCHAR": {
                            field.set(this, set.getString(field.getName()));
                            break;
                        }
                    }
                    field.setAccessible(false);
                }
            }catch (IllegalAccessException | SQLException e){
                e.printStackTrace();
            }
        }
    }

    public PlayerMySQLConfiguration() {
        this.keys = 0;
        this.trophies = 0;
        this.wins = 0;
        this.totalGames = 0;
    }

    @Override
    public String getUuid() {
        return uuid;
    }

    @Override
    public Integer getKeys() {
        return keys;
    }

    @Override
    public Integer getTrophies() {
        return trophies;
    }

    @Override
    public Integer getWins() {
        return wins;
    }

    @Override
    public Integer getTotalGames() {
        return totalGames;
    }

    @Override
    public void setKeys(Integer keys) {
        this.keys = keys;
    }

    @Override
    public void setTrophies(Integer trophies) {
        this.trophies = trophies;
    }

    @Override
    public void setWins(Integer wins) {
        this.wins = wins;
    }

    @Override
    public void setTotalGames(Integer totalGames) {
        this.totalGames = totalGames;
    }
}
