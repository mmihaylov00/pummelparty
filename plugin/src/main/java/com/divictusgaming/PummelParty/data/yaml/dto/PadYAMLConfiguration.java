package com.divictusgaming.PummelParty.data.yaml.dto;

import com.divictusgaming.PummelParty.board.Pad;
import com.divictusgaming.PummelParty.board.Padtype;
import com.divictusgaming.PummelParty.data.yaml.annotations.YAMLPath;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

public class PadYAMLConfiguration extends YAMLConfiguration {

    @YAMLPath(path = "location")
    private Location location;

    @YAMLPath(path = "type")
    private Padtype type;

    @YAMLPath(path = "children")
    private List<Integer> children;
    public PadYAMLConfiguration()
    {
    	this.children=new ArrayList<>();
    }
    public PadYAMLConfiguration(Pad pad) 
    {
        this.location = pad.getLocation();
        this.type = pad.getPadtype();
        this.children = new ArrayList<>();
    }
    
    public Pad getPad()
    {
    	//return new pad
    	return new Pad(location, type);
    }

	public void addChildren(int id)
	{
        this.children.add(id);
    }

    public Location getLocation()
    {
        return location;
    }

    public Padtype getType() 
    {
        return type;
    }

    public List<Integer> getChildren() 
    {
        return children;
    }
}
