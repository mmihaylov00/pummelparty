package com.divictusgaming.PummelParty.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class StringFormater {
    public static String format(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    //change the formatting color of all fields
    public static void formatAllFields(Object object) {
        for (Field declaredField : object.getClass().getDeclaredFields()) {
            try {
                declaredField.setAccessible(true);
                if (declaredField.getType() == String.class) {
                    declaredField.set(object, format((String) declaredField.get(object)));
                } else if (declaredField.getType() == ArrayList.class) {
                    ArrayList<String> o = (ArrayList<String>) declaredField.get(object);
                    for (int i = 0; i < o.size(); i++) {
                        o.set(i, format(o.get(i)));
                    }
                }
                declaredField.setAccessible(false);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    //method for extracting locations from string
    public static Location stringToLocation(String str) {
        String[] split = str.split(";");
        if (split.length > 4) {
            return new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]),
                    Float.parseFloat(split[4]), Float.parseFloat(split[5]));
        }
        return new Location(Bukkit.getWorld(split[0]), Double.parseDouble(split[1]), Double.parseDouble(split[2]), Double.parseDouble(split[3]));
    }

    //Make location to string
    public static String locationToString(Location loc, boolean saveYawAndPitch) {
        if (saveYawAndPitch)
            return loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ() + ";" + loc.getYaw() + ";" + loc.getPitch();
        return loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ();
    }
}
