package com.divictusgaming.PummelParty.util.particles;

import com.divictusgaming.PummelParty.util.CommonUtils;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class BorderParticles {
    public static void sendBoundary(Player player, Location corner1, Location corner2) {
        //get min coords
        double xMin = Math.min(corner1.getX(), corner2.getX());
        double yMin = Math.min(corner1.getY(), corner2.getY());
        double zMin = Math.min(corner1.getZ(), corner2.getZ());
        //get max coords
        double xMax = Math.max(corner1.getX(), corner2.getX());
        double yMax = Math.max(corner1.getY(), corner2.getY());
        double zMax = Math.max(corner1.getZ(), corner2.getZ());
        //get total blocks in region
        double totalBlocks = (xMax - xMin) * (yMax - yMin) * (zMax - zMin);
        Color color;
        if (totalBlocks <= 1000)
            color = CommonUtils.getColorFromName("gray");
        else if (totalBlocks > 1000 && totalBlocks <= 10000)
            color = CommonUtils.getColorFromName("blue");
        else if (totalBlocks > 10000 && totalBlocks <= 50000)
            color = CommonUtils.getColorFromName("green");
        else if (totalBlocks > 50000 && totalBlocks <= 100000)
            color = CommonUtils.getColorFromName("lime");
        else if (totalBlocks > 100000 && totalBlocks < 500000)
            color = CommonUtils.getColorFromName("orange");
        else
            color = CommonUtils.getColorFromName("red");
        //display particles for both locations while changing only 1 axis
        for (double i = xMin; i <= xMax; i += 0.4) {
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, i, yMax, zMax, 1, color);
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, i, yMin, zMin, 1, color);
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, i, yMax, zMin, 1, color);
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, i, yMin, zMax, 1, color);
        }
        for (double i = yMin; i <= yMax; i += 0.4) {
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, xMax, i, zMax, 1, color);
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, xMin, i, zMin, 1, color);
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, xMin, i, zMax, 1, color);
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, xMax, i, zMin, 1, color);
        }
        for (double i = zMin; i <= zMax; i += 0.4) {
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, xMax, yMax, i, 1, color);
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, xMin, yMin, i, 1, color);
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, xMin, yMax, i, 1, color);
            FastParticle.spawnParticle(player, ParticleType.REDSTONE, xMax, yMin, i, 1, color);
        }
    }
}
