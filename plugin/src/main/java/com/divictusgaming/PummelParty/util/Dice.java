package com.divictusgaming.PummelParty.util;

import com.divictusgaming.PummelParty.PummelPartyMain;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;


public class Dice {
    private static final char[] NUMBERS = {'①', '②', '③', '④', '⑤', '⑥', '⑦', '⑧'};
    private static final int ANIMATION_TICKS = 40;    //ticks after which animation is completed
    private static int timer;
    private static int time;
    private static List<UUID> animating = new ArrayList<>();
    private static Map<UUID, Integer> lastRolls = new ConcurrentHashMap<>();

    public static int getLastRoll(Player player) {
        if (!lastRolls.containsKey(player.getUniqueId()))    //add new roll if didnt have before
            lastRolls.put(player.getUniqueId(), RandomGenerator.getRandom(NUMBERS.length) + 1);
        return lastRolls.get(player.getUniqueId());
    }

    public static boolean isAnimating(Player player) {
        if (animating.contains(player.getUniqueId()))
            return true;
        return false;
    }

    public static int getAnimationTicks() {
        return ANIMATION_TICKS;
    }

    public static void animateThrowDice(Player player) {
        //I used the logic i used in my CsGo crate animation
        //put this player in animating
        animating.add(player.getUniqueId());
        //have a dice list with 5 dice rolls
        List<Integer> rolls = new ArrayList<Integer>();
        for (int i = 1; i <= 5; i++)
            rolls.add(RandomGenerator.getRandom(NUMBERS.length) + 1);
        //have a display format 
        String displayFormat = "&c%roll0% &6%roll1% &e| &a%roll2% &e| &6%roll3% &c%roll4%";
        //have to static variables used for slowing roll in time
        //start a runable for timing the animation
        timer = 1;
        time = 0;
        new BukkitRunnable() {
            int times = 1;
            int temp = 1;

            @Override
            public void run() {
                //increment timer by temp amount(logic is time need to get higher as dice gets slower);
                timer = timer + temp;
                //nested runable so time can be varied
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        //add extra item to rolls so all move forward
                        rolls.add(0, RandomGenerator.getRandom(NUMBERS.length) + 1);
                        //send roll title to player
                        //replace rolls in format
                        String toSend = displayFormat;
                        for (int i = 0; i < 5; i++)
                            toSend = toSend.replace("%roll" + i + "%", String.valueOf(NUMBERS[rolls.get(i) - 1]));
                        //send message using JsonFormat
                        JSONMessage.create(CommonUtils.chat(toSend)).actionbar(player);
                        //play a roll sound to player (i use sounds util for 1.8-1.14 sound names)
                        player.playSound(player.getLocation(), Sounds.NOTE_PLING.bukkitSound(), 10, 1);
                        if (time >= 8)    //animation ended
                        {
                            player.playSound(player.getLocation(), Sounds.ORB_PICKUP.bukkitSound(), 10, 1);
                            player.playSound(player.getLocation(), Sounds.ORB_PICKUP.bukkitSound(), 10, 2);
                            player.playSound(player.getLocation(), Sounds.ORB_PICKUP.bukkitSound(), 10, 3);
                            //whatever rolls contains at index 2 is the dice the player rolled
                            //to send this ill store this in a static map & get value with new function
                            lastRolls.put(player.getUniqueId(), rolls.get(2));
                            //remove the player from animating
                            animating.remove(player.getUniqueId());
                        }
                        //increment time
                        time = time + 1;
                    }
                }.runTaskLater(PummelPartyMain.getPlugin(), timer);
                if (timer >= 20) {
                    this.cancel();
                }
                times++;
                if (times >= 5)
                    temp++;
            }
        }.runTaskTimer(PummelPartyMain.getPlugin(), 0, 0);
        //done play a title bar animation
        //done need an easy title message sending implementation
    }

}