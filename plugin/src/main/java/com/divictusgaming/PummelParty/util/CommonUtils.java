package com.divictusgaming.PummelParty.util;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.divictusgaming.PummelParty.util.NBT.SafeNBT;

import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.util.*;
import java.util.Map.Entry;


public class CommonUtils {
    //function to get rgb color from color name (cause MC colors are shit :D)
    public static Color getColorFromName(String name) {
        HashMap<String, Color> codes = new HashMap<>();
        codes.put("light_blue", Color.fromRGB(30, 144, 255));
        codes.put("black", Color.fromRGB(105, 105, 105));
        codes.put("cyan", Color.fromRGB(135, 206, 235));
        codes.put("magenta", Color.fromRGB(255, 0, 255));
        codes.put("gray", Color.fromRGB(211, 211, 211));
        codes.put("green", Color.fromRGB(34, 139, 34));
        codes.put("lime", Color.fromRGB(50, 205, 50));
        codes.put("brown", Color.fromRGB(139, 69, 19));
        codes.put("blue", Color.fromRGB(75, 0, 130));
        codes.put("orange", Color.fromRGB(255, 165, 0));
        codes.put("purple", Color.fromRGB(128, 0, 128));
        codes.put("red", Color.RED);
        codes.put("white", Color.fromRGB(255, 255, 255));
        codes.put("yellow", Color.fromRGB(255, 215, 0));
        codes.put("light_gray", Color.fromRGB(47, 79, 79));
        name = name.toLowerCase();
        name = name.replace(" ", "_");
        return codes.getOrDefault(name, Color.WHITE);
    }

    //returns the server version as an double (easier to check for higher versions)
    public static double getServerVersion() {
        String version = Bukkit.getServer().getBukkitVersion().substring(2, 5);
        return Double.parseDouble(version);
    }

    //formats the time into dd days HH hours mm min
    //or HH hours mm min ss sec
    //or mm min ss sec
    //depending on the time given
    public static String getTimeFormat(int seconds) {
        String total = "";
        if (seconds >= 86400) // atleast a day
        {
            int day = (int) toDays(seconds);
            int hour = (int) toHours(seconds);
            int min = (int) toMin(seconds);
            total = day + " days " + hour + " hours " + min + " mins";
        } else if (seconds >= 3600) //atleast an hour
        {
            int hour = (int) toHours(seconds);
            int min = (int) toMin(seconds);
            int sec = toSec(seconds);
            total = hour + " hours " + min + " mins " + sec + " secs";
        } else //a min or less
        {
            int min = (int) toMin(seconds);
            int sec = toSec(seconds);
            total = min + " mins " + sec + " secs";
        }

        return total;
    }

    //extra functions for above logic
    private static int toSec(double total) {
        double temp = ((total / 86400.0D - toDays(total)) * 24.0D - toHours(total)) * 60 - toMin(total);
        return (int) Math.floor(temp * 60);
    }

    private static double toMin(double total) {
        double temp = (total / 86400.0D - toDays(total)) * 24.0D - toHours(total);
        return Math.floor(temp * 60.0D);
    }

    private static double toHours(double total) {
        double temp = total / 86400.0D - toDays(total);
        return Math.floor(temp * 24.0D);
    }

    private static double toDays(double total) {
        return Math.floor(total / 86400.0D);
    }

    //gets player's total experience (stolen from essentials :D)
    public static float getExperience(Player player) {
        int exp = Math.round(getExpAtLevel(player.getLevel()) * player.getExp());
        int currentLevel = player.getLevel();

        while (currentLevel > 0) {
            currentLevel--;
            exp += getExpAtLevel(currentLevel);
        }
        if (exp < 0) {
            exp = Integer.MAX_VALUE;
        }
        return exp;
    }

    //sets player's Experience
    public static void setTotalExperience(Player player, int exp) {
        player.setExp(0.0F);
        player.setLevel(0);
        player.setTotalExperience(0);

        int amount = exp;
        while (amount > 0) {
            int expToLevel = getExpAtLevel(player.getLevel());
            amount -= expToLevel;
            if (amount >= 0) {

                player.giveExp(expToLevel);
                continue;
            }
            amount += expToLevel;
            player.giveExp(amount);
            amount = 0;
        }
    }

    //extra fuction for above logic
    private static int getExpAtLevel(int level) {
        if (level <= 15) {
            return 2 * level + 7;
        }
        if (level <= 30) {
            return 5 * level - 38;
        }
        return 9 * level - 158;
    }

    //returns item's name from itemstack
    public static String getItemName(ItemStack item) {
        if (item.hasItemMeta() && item.getItemMeta().hasDisplayName())
            return item.getItemMeta().getDisplayName();
        else {
            String name = "";
            for (String s : item.getType().name().split("_"))
                name = name + s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase() + " ";
            return name.substring(0, name.length() - 1);
        }
    }

    //returns a string with prefix format(used to display bigger values)
    public static String getPrefixFormatted(long value) {
        //Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
        if (value == Long.MIN_VALUE) return getPrefixFormatted(Long.MIN_VALUE + 1);
        if (value < 0) return "-" + getPrefixFormatted(-value);
        if (value < 1000) return Long.toString(value); //deal with easy case

        Entry<Long, String> e = suffixes.floorEntry(value);
        Long divideBy = e.getKey();
        String suffix = e.getValue();

        long truncated = value / (divideBy / 10); //the number part of the output times 10
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
    }

    //map for upper function
    @SuppressWarnings({"unchecked", "rawtypes"})
    private static final NavigableMap<Long, String> suffixes = new TreeMap();

    static {
        suffixes.put(1_000L, "k");
        suffixes.put(1_000_000L, "M");
        suffixes.put(1_000_000_000L, "G");
        suffixes.put(1_000_000_000_000L, "T");
        suffixes.put(1_000_000_000_000_000L, "P");
        suffixes.put(1_000_000_000_000_000_000L, "E");
    }

    //adds decimals to a number (commonly used for currency)
    public static String getDecimalFormatted(long amount) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
        formatter.setGroupingUsed(true);
        return formatter.format(amount).replace("$", "");
    }

    //gives item to player (drops item on player if his inv is full)
    public static void giveItem(Player player, ItemStack item) {
        if (item == null)
            return;
        if (player.getInventory().firstEmpty() < 0)
            player.getWorld().dropItem(player.getLocation(), item);
        else
            player.getInventory().addItem(item);
    }

    //gets an itemstack from an existing node in a yml file
    //if u save itemstack at ingame-item on node that is used
    //else nodes type,name&lore are used
    //this needs the following utilities to function properly
    //Umaterials	(used for itemstack support from 1.8 to 1.15)
    //SkullCreator  (used to generate custom skulls from online ids)
    public static ItemStack getItemFromNode(ConfigurationSection node) {
        if (node.isSet("ingame-item"))
            return node.getItemStack("ingame-item");
        else if (node.isSet("type"))
            return getCustomItem(node.getString("type"), node.getString("name"), node.getStringList("lore"));
        return null;
    }

    //extra function for above login (u can use this seperately if u want)
    @SuppressWarnings("deprecation")
    public static ItemStack getCustomItem(String type, String name, List<String> lore) {
        boolean glow = false;
        if (type.startsWith("<glow>")) {
            glow = true;
            type = type.replace("<glow>", "");
        }
        ItemStack item = UMaterials.AIR.getItemStack();
        if (type.startsWith("<skull>")) {
            String sname = type.replace("<skull>", "");
            item = UMaterials.PLAYER_HEAD_ITEM.getItemStack();
            SkullMeta sm = (SkullMeta) item.getItemMeta();
            sm.setOwner(sname);
            item.setItemMeta(sm);
        } else if (type.startsWith("<id>"))
            item = SkullCreator.itemFromBase64(type.replace("<id>", ""));
        else
            item = UMaterials.valueOf(type.toUpperCase()).getItemStack();
        ItemMeta im = item.getItemMeta();
        if (name != null) {
            im.setDisplayName(CommonUtils.chat(name));
        }
        if (lore != null && lore.size() > 0) {
            ArrayList<String> l = new ArrayList<String>();
            for (String s : lore)
                l.add(CommonUtils.chat(s));
            im.setLore(l);
        }
        item.setItemMeta(im);
        if (glow) {
            item.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 4341);
            ItemMeta ime = item.getItemMeta();
            ime.addItemFlags(new ItemFlag[]{ItemFlag.HIDE_ENCHANTS});
            item.setItemMeta(ime);
        }
        return item;

    }

    //this uses the JSONMessage util & an algorithm i created to send jsom message directly to a player
	/*
	for a message the following are special characters
	# <p> is to split msg into seperate parts
	# <t> is to add tooltip
	# <n> is for new line (only for tooltip)
	# <cs> is to suggest cmd on click
	# <cc> is to execute cmd on click
	# <delay>10 is to give delay btw messages
	# <st>fadein:time:fadeout<st> to send it as title (only at the end)
	# <sst> to send it as a subtitle(only works if there is a title being displayed)
	# <sh> to send it above hotbar (only at end)
	# for example 
	# this is a simple msg <p> this is a new part <t> this msg will <n> have this tooltip <t><cs>/this cmd is suggested<cs> */
    //this is really easier then writing json string
    public static void sendJSONMessage(Player to, Player of, JavaPlugin plugin, List<String> message) {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (String data : message) {
                    if (data.startsWith("<delay>")) {
                        data = data.replace("<delay>", "");
                        int delay = 0;
                        try {
                            delay = Integer.valueOf(data);
                        } catch (NumberFormatException | NullPointerException e) {
                        }
                        try {
                            Thread.sleep(delay);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        continue;
                    }
                    if (data.startsWith("<cmd>")) {
                        data = data.replace("<cmd>", "");
                        Bukkit.dispatchCommand(to, data);
                        continue;
                    }
                    sendHelp(to, data);
                }
            }
        }.runTaskAsynchronously(plugin);
    }

    //extra part of above logic
    private static void sendHelp(Player player, String data) {
        JSONMessage msg = JSONMessage.create();
        ArrayList<Integer> title = null;
        boolean subtitle = false;
        String hotbar = null;
        for (String part : data.split("<p>")) {
            JSONMessage jtip = null;
            String suggest = null;
            String cmd = null;
            if (part.contains("<t>") && getCount(part, "<t>") == 2) {
                jtip = JSONMessage.create();
                String tip = part.split("<t>")[1];
                part = part.replace(tip, "").replace("<t>", "");
                String[] array = tip.split("<n>");
                for (int i = 0; i < array.length; i++) {
                    jtip = jtip.then(CommonUtils.chat(array[i]));
                    if (i < array.length - 1)
                        jtip = jtip.newline();
                }
            }
            if (part.contains("<cs>") && getCount(part, "<cs>") == 2) {
                String tip = part.split("<cs>")[1];
                part = part.replace(tip, "").replace("<cs>", "");
                suggest = tip;
            } else if (part.contains("<cc>") && getCount(part, "<cc>") == 2) {
                String tip = part.split("<cc>")[1];
                part = part.replace(tip, "").replace("<cc>", "");
                cmd = tip;
            }
            if (part.endsWith("<st>") && getCount(part, "<st>") == 2) {
                String tip = part.split("<st>")[1];
                part = part.replace(tip, "").replace("<st>", "");
                tip = tip.replace(" ", "");
                String[] settings = tip.split(":");
                if (settings.length == 3) {
                    title = new ArrayList<Integer>();
                    for (String s : settings) {
                        try {
                            title.add(Integer.valueOf(s));
                        } catch (NumberFormatException | NullPointerException e) {
                            title.add(0);
                        }
                    }
                }
            } else if (part.endsWith("<sst>")) {
                part = part.replace("<sst>", "");
                subtitle = true;
            } else if (part.endsWith("<sh>")) {
                part = part.replace("<sh>", "");
                hotbar = part;
            }
            msg = msg.then(CommonUtils.chat(part));
            if (jtip != null)
                msg = msg.tooltip(jtip);
            if (suggest != null && suggest != "")
                msg = msg.suggestCommand(suggest);
            else if (cmd != null && cmd != "")
                msg = msg.runCommand(cmd);
        }
        if (title != null && title.size() == 3)
            msg.title(title.get(0), title.get(1), title.get(2), player);
        else if (subtitle)
            msg.subtitle(player);
        else if (hotbar != null)
            JSONMessage.actionbar(CommonUtils.chat(hotbar), player);
        else
            msg.send(player);
    }

    private static int getCount(String string, String regex) {
        int count = StringUtils.countMatches(string, regex);
        return count;
    }

    //used to translate & to color
    public static String chat(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    //used to check chance of something
    public static boolean getChance(int max, int chance) {
        Random r = new Random();
        int d = r.nextInt(max) + 1;
        if (d <= chance)
            return true;
        return false;
    }

    //these use NBTEditor to function
    //I use them to store data inside items
    //its alot better to use nbt then name or lore
    public static String getStringTag(ItemStack item) {
        String tag = "";
        SafeNBT nbtTag = SafeNBT.get(item);
        if (nbtTag.getString("PummelParty-String") != null) {
            tag = nbtTag.getString("PummelParty-String");
        }
        return tag;

    }

    public static ItemStack setStringTag(ItemStack item, String tag) {
        SafeNBT nbtTag = SafeNBT.get(item);
        nbtTag.setString("PummelParty-String", tag);
        item = nbtTag.apply(item);
        return item;
    }

    public static String getExtraStringTag(ItemStack item) {
        String tag = "";
        SafeNBT nbtTag = SafeNBT.get(item);
        if (nbtTag.getString("PummelParty-ExtraString") != null) {
            tag = nbtTag.getString("PummelParty-ExtraString");
        }
        return tag;

    }

    public static ItemStack setExtraStringTag(ItemStack item, String tag) {
        SafeNBT nbtTag = SafeNBT.get(item);
        nbtTag.setString("PummelParty-ExtraString", tag);
        item = nbtTag.apply(item);
        return item;
    }

    //used to unregister any command from any plugin (i use this if my cmd conflict with any other plugin)
    @SuppressWarnings("unchecked")
    public static void unRegisterCommand(String cmd, JavaPlugin plugin) {
        try {
            PluginCommand command = (PluginCommand) plugin.getCommand(cmd);
            Object result = getPrivateField(Bukkit.getPluginManager(), "commandMap");
            CommandMap commandMap = (CommandMap) result;
            HashMap<String, Command> knownCommands = (HashMap<String, Command>)
                    getPrivateField(commandMap, "knownCommands");
            knownCommands.remove(command.getName());
            for (String alias : command.getAliases()) {
                if (knownCommands.containsKey(alias) &&
                        knownCommands.get(alias).toString().contains(plugin.getName())) {
                    knownCommands.remove(alias);
                }
            }
        } catch (Exception e) {

        }
    }

    //get head of any player
    @SuppressWarnings("deprecation")
    public static ItemStack getSkull(Player player) {
        ItemStack skull = UMaterials.PLAYER_HEAD_ITEM.getItemStack();
        SkullMeta sm = (SkullMeta) skull.getItemMeta();
        sm.setOwner(player.getName());
        sm.setDisplayName(player.getName() + "'s head");
        skull.setItemMeta(sm);
        return skull;
    }

    @SuppressWarnings("deprecation")
    public static ItemStack getSkull(String player) {
        ItemStack skull = UMaterials.PLAYER_HEAD_ITEM.getItemStack();
        SkullMeta sm = (SkullMeta) skull.getItemMeta();
        sm.setOwner(player);
        sm.setDisplayName(player + "'s head");
        skull.setItemMeta(sm);
        return skull;
    }

    //used to evaluate arithematic expressions & get a double value (this is too usefull)
    //expressions like (2*100)/50 etc
    public static double evaluateString(String str) {
        return new Object() {
            int pos = -1, ch;

            void nextChar() {
                ch = (++pos < str.length()) ? str.charAt(pos) : -1;
            }

            boolean eat(int charToEat) {
                while (ch == ' ') nextChar();
                if (ch == charToEat) {
                    nextChar();
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char) ch);
                return x;
            }

            // Grammar:
            // expression = term | expression `+` term | expression `-` term
            // term = factor | term `*` factor | term `/` factor
            // factor = `+` factor | `-` factor | `(` expression `)`
            //        | number | functionName factor | factor `^` factor

            double parseExpression() {
                double x = parseTerm();
                for (; ; ) {
                    if (eat('+')) x += parseTerm(); // addition
                    else if (eat('-')) x -= parseTerm(); // subtraction
                    else return x;
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for (; ; ) {
                    if (eat('*')) x *= parseFactor(); // multiplication
                    else if (eat('/')) x /= parseFactor(); // division
                    else return x;
                }
            }

            double parseFactor() {
                if (eat('+')) return parseFactor(); // unary plus
                if (eat('-')) return -parseFactor(); // unary minus

                double x;
                int startPos = this.pos;
                if (eat('(')) { // parentheses
                    x = parseExpression();
                    eat(')');
                } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
                    while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
                    x = Double.parseDouble(str.substring(startPos, this.pos));
                } else if (ch >= 'a' && ch <= 'z') { // functions
                    while (ch >= 'a' && ch <= 'z') nextChar();
                    String func = str.substring(startPos, this.pos);
                    x = parseFactor();
                    if (func.equals("sqrt")) x = Math.sqrt(x);
                    else if (func.equals("sin")) x = Math.sin(Math.toRadians(x));
                    else if (func.equals("cos")) x = Math.cos(Math.toRadians(x));
                    else if (func.equals("tan")) x = Math.tan(Math.toRadians(x));
                    else throw new RuntimeException("Unknown function: " + func);
                } else {
                    throw new RuntimeException("Unexpected: " + (char) ch);
                }

                if (eat('^')) x = Math.pow(x, parseFactor()); // exponentiation

                return x;
            }
        }.parse();
    }

    private static Object getPrivateField(Object object, String field) throws SecurityException,
            NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = object.getClass();
        Field objectField = clazz.getDeclaredField(field);
        objectField.setAccessible(true);
        return objectField.get(object);
    }
}
