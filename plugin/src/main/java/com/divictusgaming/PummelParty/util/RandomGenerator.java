package com.divictusgaming.PummelParty.util;

import java.security.SecureRandom;

public class RandomGenerator {
    private static SecureRandom rnd = new SecureRandom();

    public static boolean checkPercent(Double percent) {
        return rnd.nextDouble() * 100 + percent > 100;
    }

    public static int getRandom(Integer max) {
        return rnd.nextInt(max);
    }
}
