package com.divictusgaming.PummelParty.util;

import com.google.common.io.ByteStreams;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;


public class YMLManager {
    private FileConfiguration file;
    private JavaPlugin plugin;
    private File loc;
    private String name;
    private String location;

    //load a .yml file with name (use this for config maybe)
    public YMLManager(JavaPlugin plugin, String name) {
        this.name = name;
        this.location = null;
        this.plugin = plugin;
        load();
    }

    //load a .yml file with name & location
    public YMLManager(JavaPlugin plugin, String name, String location) {
        this.name = name;
        this.location = location;
        this.plugin = plugin;
        load();
    }

    //load a .yml file with name & location & save it with newName
    public YMLManager(JavaPlugin plugin, String name, String location, String newName) {
        this.name = name;
        this.location = location;
        this.plugin = plugin;
        load(newName);
        this.name = newName;
    }

    //load a .yml file with name & location & save it with newName at newLocation
    public YMLManager(JavaPlugin plugin, String name, String location, String newName, String newLocation) {
        this.name = name;
        this.location = location;
        this.plugin = plugin;
        load(newName, newLocation);
        this.name = newName;
        this.location = newLocation;
    }

    //delete this file
    public void delete() {
        loc.delete();
    }

    //get the file
    public FileConfiguration getFile() {
        return file;
    }

    //save this file
    public boolean save() {
        try {
            file.save(loc);
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    //save this file at a new location
    public void save(File loc2) {
        try {
            file.save(loc2);
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    //reload this file from plugin folder
    public void reload() {
        if (location == null) {
            if (!plugin.getDataFolder().exists()) {
                plugin.getDataFolder().mkdir();
            }
            loc = new File(plugin.getDataFolder(), name + ".yml");
            if (!loc.exists()) {
                try {
                    loc.createNewFile();
                    try (InputStream is = plugin.getResource(name + ".yml");
                         OutputStream os = new FileOutputStream(loc)) {
                        ByteStreams.copy(is, os);
                    }
                } catch (IOException e) {

                }
            }
            loc = new File(plugin.getDataFolder(), name + ".yml");
            file = YamlConfiguration.loadConfiguration(loc);
        } else {
            if (!plugin.getDataFolder().exists()) {
                plugin.getDataFolder().mkdir();
            }
            loc = new File(plugin.getDataFolder() + location);
            if (!loc.exists())
                loc.mkdir();
            loc = new File(plugin.getDataFolder() + location, name + ".yml");
            if (!loc.exists()) {
                try {
                    loc.createNewFile();
                    try (InputStream is = plugin.getClass().getResourceAsStream(location + "/" + name + ".yml");
                         OutputStream os = new FileOutputStream(loc)) {
                        ByteStreams.copy(is, os);
                    }
                } catch (IOException e) {

                }
            }
            loc = new File(plugin.getDataFolder() + location, name + ".yml");
            file = YamlConfiguration.loadConfiguration(loc);
        }
    }

    //load this file again from within the plugin(doesnt work with newName &newLocations)
    public void refresh() {
        if (location == null) {
            if (!plugin.getDataFolder().exists()) {
                plugin.getDataFolder().mkdir();
            }
            loc = new File(plugin.getDataFolder(), name + ".yml");
            try {
                loc.createNewFile();
                try (InputStream is = plugin.getResource(name + ".yml");
                     OutputStream os = new FileOutputStream(loc)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException e) {

            }
            loc = new File(plugin.getDataFolder(), name + ".yml");
            file = YamlConfiguration.loadConfiguration(loc);
        } else {
            if (!plugin.getDataFolder().exists()) {
                plugin.getDataFolder().mkdir();
            }
            loc = new File(plugin.getDataFolder() + location);
            if (!loc.exists())
                loc.mkdir();
            loc = new File(plugin.getDataFolder() + location, name + ".yml");
            try {
                loc.createNewFile();
                try (InputStream is = plugin.getClass().getResourceAsStream(location + "/" + name + ".yml");
                     OutputStream os = new FileOutputStream(loc)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException e) {

            }
            loc = new File(plugin.getDataFolder() + location, name + ".yml");
            file = YamlConfiguration.loadConfiguration(loc);
        }
    }

    private void load(String newName, String newLocation) {
        if (!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdir();
        }
        loc = new File(plugin.getDataFolder() + newLocation);
        if (!loc.exists())
            loc.mkdir();
        loc = new File(plugin.getDataFolder() + newLocation, newName + ".yml");
        if (!loc.exists()) {
            try {
                loc.createNewFile();
                if (location == null) {
                    try (InputStream is = plugin.getResource(name + ".yml");
                         OutputStream os = new FileOutputStream(loc)) {
                        ByteStreams.copy(is, os);
                    }
                } else {

                    try (InputStream is = plugin.getClass().getResourceAsStream(location + "/" + name + ".yml");
                         OutputStream os = new FileOutputStream(loc)) {
                        ByteStreams.copy(is, os);
                    }
                }
            } catch (IOException e) {

            }
        }
        loc = new File(plugin.getDataFolder() + newLocation, newName + ".yml");
        file = YamlConfiguration.loadConfiguration(loc);
    }

    private void load(String newName) {
        if (location == null) {
            if (!plugin.getDataFolder().exists()) {
                plugin.getDataFolder().mkdir();
            }
            loc = new File(plugin.getDataFolder(), newName + ".yml");
            if (!loc.exists()) {
                try {
                    loc.createNewFile();
                    try (InputStream is = plugin.getResource(name + ".yml");
                         OutputStream os = new FileOutputStream(loc)) {
                        ByteStreams.copy(is, os);
                    }
                } catch (IOException e) {

                }
            }
            loc = new File(plugin.getDataFolder(), newName + ".yml");
            file = YamlConfiguration.loadConfiguration(loc);
        } else {
            if (!plugin.getDataFolder().exists()) {
                plugin.getDataFolder().mkdir();
            }
            loc = new File(plugin.getDataFolder() + location);
            if (!loc.exists())
                loc.mkdir();
            loc = new File(plugin.getDataFolder() + location, newName + ".yml");
            if (!loc.exists()) {
                try {
                    loc.createNewFile();
                    try (InputStream is = plugin.getClass().getResourceAsStream(location + "/" + name + ".yml");
                         OutputStream os = new FileOutputStream(loc)) {
                        ByteStreams.copy(is, os);
                    }
                } catch (IOException e) {

                }
            }
            loc = new File(plugin.getDataFolder() + location, newName + ".yml");
            file = YamlConfiguration.loadConfiguration(loc);
        }
    }

    private void load() {
        if (location == null) {
            if (!plugin.getDataFolder().exists()) {
                plugin.getDataFolder().mkdir();
            }
            loc = new File(plugin.getDataFolder(), name + ".yml");
            if (!loc.exists()) {
                try {
                    loc.createNewFile();
                    try (InputStream is = plugin.getResource(name + ".yml");
                         OutputStream os = new FileOutputStream(loc)) {
                        ByteStreams.copy(is, os);
                    }
                } catch (IOException e) {

                }
            }
            loc = new File(plugin.getDataFolder(), name + ".yml");
            file = YamlConfiguration.loadConfiguration(loc);
        } else {
            if (!plugin.getDataFolder().exists()) {
                plugin.getDataFolder().mkdir();
            }
            loc = new File(plugin.getDataFolder() + location);
            if (!loc.exists())
                loc.mkdir();
            loc = new File(plugin.getDataFolder() + location, name + ".yml");
            if (!loc.exists()) {
                try {
                    loc.createNewFile();
                    try (InputStream is = plugin.getClass().getResourceAsStream(location + "/" + name + ".yml");
                         OutputStream os = new FileOutputStream(loc)) {
                        ByteStreams.copy(is, os);
                    }
                } catch (IOException e) {

                }
            }
            loc = new File(plugin.getDataFolder() + location, name + ".yml");
            file = YamlConfiguration.loadConfiguration(loc);
        }
    }
}
