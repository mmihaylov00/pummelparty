package com.divictusgaming.PummelParty.util;

import com.divictusgaming.PummelParty.PummelPartyMain;
import com.divictusgaming.PummelParty.board.Board;
import com.divictusgaming.PummelParty.characters.PlayerCharacter;
import com.divictusgaming.PummelParty.dto.ScoreboardObjectDTO;
import com.divictusgaming.PummelParty.enums.ScoreboardType;
import com.divictusgaming.PummelParty.data.yaml.dto.ConfigYAMLConfiguration;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.*;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScoreboardManager {
    //TODO make scoreboards for minigames & game
    //Use UUID instead of Player object in collections!
    private final Map<UUID, ScoreboardObjectDTO> activeBoards;
    private final ChatColor[] colors = ChatColor.values();

    public ScoreboardManager() {
        activeBoards = new HashMap<>();
        //clock to update all running scores per second
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    for (UUID player : activeBoards.keySet()) {
                        updateScoreboard(player);
                    }
                    //if something has changed in the scoreboard, we don't want to break everything
                } catch (ConcurrentModificationException e) {
                }
            }
        }.runTaskTimer(PummelPartyMain.getPlugin(), 0, 20);
    }

    private void updateScoreboard(UUID uuid) {
        ScoreboardObjectDTO b = activeBoards.get(uuid);
        //sometimes we don't want to update the lines
        //for example there is no need to update the scoreboard in the lobby every time
        //if we want to do it once, we set forceUpdate to true (for example when we open a box)
        if (!b.isForceUpdate()) if (!b.isDoUpdate()) return;

        int boardPlayers = 0;
        //we get player info
        PlayerCharacter playerCharacter = PummelPartyMain.getCharacterStorage().getCharacter(uuid);
        if (playerCharacter == null) return;

        Board board = playerCharacter.getBoard();
        if (board != null) {
            boardPlayers = board.getPlayers().length;
        }

        //we update only the lines with placeholders
        for (Map.Entry<Team, String> e : b.getTeams().entrySet()) {
            //we replace the variables
            String line = replacePlaceholders(e.getValue(), uuid);
            //if there is a {player} placeholders, that's used in the teamLine, we replace it
            //we keep the track for the player in the boardPlayers var
            if (boardPlayers > 0 && line.contains("{player}")) {
                //getTeams is reversed, so we want to set them from the back to the start
                //so we can keep the order
                try {
                    line = replaceTeamLines(line, uuid, board.getPlayers().length - (boardPlayers--));
                } catch (IndexOutOfBoundsException ex) {
                    //if a player left the game, we want to replace the scoreboard
                    setScoreboard(uuid, ScoreboardType.IN_GAME);
                }
            }
            //if more than 16 symbols we gotta split it in 2 and add the second half as a suffix
            if (line.length() > 16) {
                String prefix = line.substring(0, 16);
                String suffix = line.substring(16);

                //we don't want to separate the color code and break it
                if (prefix.endsWith("§")) {
                    prefix = prefix.substring(0, 15);
                    suffix = "§" + suffix;
                }

                suffix = getSuffixColor(prefix, suffix);
                //if the suffix is too long we still gotta cut it
                if (suffix.length() > 16) e.getKey().setSuffix(suffix.substring(0, 16));
                else e.getKey().setSuffix(suffix);
                line = prefix;
            } else {
                e.getKey().setSuffix("");
            }
            e.getKey().setDisplayName("test");
            e.getKey().setPrefix(line);
        }
    }

    //we get the first three formats and place it to the suffix
    private String getSuffixColor(String prefix, String suffix) {
        final String regex = "(§[a-o0-9r])+";

        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(prefix);

        String last = "";

        //we find the last color group in the prefix
        while (matcher.find()) {
            last = matcher.group(0);
        }

        //if the suffix starts with a color and the prefix doesn't
        //end with a color we don't want to place the starting color there
        if (suffix.startsWith("§") && !prefix.endsWith(last)) {
            //special symbols still needs the last color before it
            if (suffix.charAt(1) < 'k' || suffix.charAt(1) > 'o') return suffix;
        }

        return last + suffix;
    }

    //used in the reload command, remove all boards
    public void reloadScoreboards() {
        for (UUID uuid : activeBoards.keySet()) {
            try {
                Bukkit.getPlayer(uuid).getScoreboard().getObjective("PummelParty").unregister();
            } catch (NullPointerException ignored) {
            }
        }
        activeBoards.clear();
    }

    //remove the board for a single player
    public void removeScoreboard(UUID p) {
        activeBoards.remove(p);
        try {
            Bukkit.getPlayer(p).getScoreboard().getObjective("PummelParty").unregister();
        } catch (NullPointerException ignored) {
        }
    }

    //a method to set the player's scoreboard
    public void setScoreboard(UUID player, ScoreboardType type) {
        removeScoreboard(player);
        ConfigYAMLConfiguration config = PummelPartyMain.getConfiguration();
        //we send them as new array list every time so they won't get modified
        switch (type) {
            case LOBBY:
                createScoreboard(player, new ArrayList<>(config.getSbLobbyLines()), config.getSbLobbyTitle(), true);
                break;
            case WAITING_LOBBY:
                createScoreboard(player, new ArrayList<>(config.getSbWaitingLobbyLines()), config.getSbWaitingLobbyTitle(), true);
                break;
            case IN_GAME:
                createScoreboard(player, new ArrayList<>(config.getSbIngameLines()), config.getSbIngameTitle(), true);
                break;
            case MINIGAME:
        }
    }

    private void createScoreboard(UUID player, ArrayList<String> sbLines, String title, boolean doUpdate) {
        //we create a new scoreboard
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective obj = scoreboard.registerNewObjective("PummelParty", "dummy", "dummy");


        HashMap<Team, String> teams = new HashMap<>();
        editTitle(obj, title);
        int hasTeamLines = 0;
        //we loop the lines
        for (int i = 0; i < sbLines.size(); i++) {
            //we don't want to go over the scoreboard limit
            if (i == 15) break;
            String line = sbLines.get(i);
            //if the line contains a placeholder
            if (line.contains("{")) {
                if (line.equals("{teamLines}")) {
                    Board playerBoard = PummelPartyMain.getCharacterStorage().getCharacter(player).getBoard();
                    ConfigYAMLConfiguration config = PummelPartyMain.getConfiguration();
                    sbLines.remove(i);
                    hasTeamLines = playerBoard.getPlayers().length;
                    for (int j = 0; j < hasTeamLines; j++) {
                        sbLines.add(i, config.getTeamLine());
                    }
                    i--;
                    continue;
                }
                if (line.equals("{startInLines}")) {
                    Board playerBoard = PummelPartyMain.getCharacterStorage().getCharacter(player).getBoard();
                    ConfigYAMLConfiguration config = PummelPartyMain.getConfiguration();
                    sbLines.remove(i);
                    if (playerBoard.getPlayers().length >= playerBoard.getMinPlayers())
                        sbLines.addAll(i, config.getSbWaitingLobbyStartingInLines());
                    else
                        sbLines.addAll(i, config.getSbWaitingLobbyNotEnoughPlayers());
                    i--;
                    continue;
                }
                //we use a unique color
                String entry = colors[i] + "" + ChatColor.RESET;
                //create the team, the name doesn't seem to matter, but I still made it unique
                Team placeholderLine = scoreboard.registerNewTeam(player.toString() + "PPLINE" + i);
                //we add the entry
                placeholderLine.addEntry(entry);
                //we replace the variables
                teams.put(placeholderLine, line);
                if (hasTeamLines > 0) {
                    line = replaceTeamLines(line, player, hasTeamLines--);
                }
                line = replacePlaceholders(line, player);
                //if more than 16 symbols we gotta split it in 2 and add the second half as a suffix
                if (line.length() > 16) {
                    String prefix = line.substring(0, 16);
                    String suffix = line.substring(16);
                    //we don't want to separate the color code and break it
                    if (prefix.endsWith("§")) {
                        prefix = prefix.substring(0, 15);
                        suffix = "§" + suffix;
                    }

                    suffix = getSuffixColor(prefix, suffix);
                    //if the suffix is too long we still gotta cut it
                    if (suffix.length() > 16) placeholderLine.setSuffix(suffix.substring(0, 16));
                    else placeholderLine.setSuffix(suffix);
                    line = prefix;
                } else {
                    placeholderLine.setSuffix("");
                }
                placeholderLine.setPrefix(line);
                obj.getScore(entry).setScore(15 - i);
                //we add it to the teams to refresh it
            } else {
                if (line.length() > 32)
                    line = line.substring(0, 32);

                Score lineScore = obj.getScore(line);
                lineScore.setScore(15 - i);
            }
        }
        Player p = Bukkit.getPlayer(player);
        if (p != null) {
            activeBoards.put(player, new ScoreboardObjectDTO(scoreboard, teams, doUpdate));
            p.setScoreboard(scoreboard);
        }
    }

    //we replace the Config#teamLine line
    private String replaceTeamLines(String line, UUID player, int current) {
        //we get the needed info
        PlayerCharacter character = PummelPartyMain.getCharacterStorage().getCharacter(player);
        Board board = character.getBoard();
        UUID[] players = board.getPlayers();

        Player linePlayer = Bukkit.getPlayer(players[players.length - current]);
        if (linePlayer == null) {
            //todo implement player has left the game
            return "";
        }
        PlayerCharacter lineChar = PummelPartyMain.getCharacterStorage().getCharacter(linePlayer);
        //we replace the variables
        if (linePlayer.getUniqueId() == player) {
            line = line.replaceAll("\\{player}", PummelPartyMain.getConfiguration().getYou());
        }
        return line.replaceAll("\\{place}", (1 + players.length - current) + "")
                .replaceAll("\\{player}", linePlayer.getName())
                .replaceAll("\\{keys}", lineChar.getGameKeys() + "")
                .replaceAll("\\{playerColor}", lineChar.getGameColor() + "")
                .replaceAll("\\{trophies}", lineChar.getGameTrophies() + "");
    }

    //replace the placeholders in the line
    private String replacePlaceholders(String line, UUID player) {
        PlayerCharacter character = PummelPartyMain.getCharacterStorage().getCharacter(player);
        ConfigYAMLConfiguration config = PummelPartyMain.getConfiguration();
        line = line.replaceAll("\\{totalKeys}", character.getTotalKeys() + "")
                .replaceAll("\\{keysSymbol}", config.getKeySymbol())
                .replaceAll("\\{totalTrophies}", character.getTotalTrophies() + "")
                .replaceAll("\\{trophiesSymbol}", config.getTrophySymbol())
                .replaceAll("\\{wins}", character.getWins() + "")
                .replaceAll("\\{time}", new SimpleDateFormat("HH:mm:ss dd.MM.yyyy").format(new Date()))
                .replaceAll("\\{gamesPlayed}", character.getTotalGames() + "")
                .replaceAll("\\{keys}", character.getGameKeys() + "");
        Board board = character.getBoard();
        if (board != null) {
            if (line.contains("{order}")) {
                if (board.getQueue().isEmpty()) {
                    line = line.replaceAll("\\{order}", config.getNoOrder());
                } else {
                    String symbol = config.getOrderSymbol();
                    StringBuilder order = new StringBuilder();
                    for (PlayerCharacter playerCharacter : board.getQueue()) {
                        order.append(playerCharacter.getGameColor()).append(symbol);
                    }
                    line = line.replaceAll("\\{order}", order.toString());
                }
            }
            line = line.replaceAll("\\{current}", board.getPlayers().length + "")
                    .replaceAll("\\{max}", board.getMaxPlayers() + "")
                    .replaceAll("\\{timer}", board.getGameTimer() + "")
                    .replaceAll("\\{playersNeeded}", board.getMinPlayers() - board.getPlayers().length + "")
                    .replaceAll("\\{trophies}", character.getGameTrophies() + "")
            ;


        }
        //If placeholderApi is enabled, we replace placeholders from it too
        if (PummelPartyMain.isPapiEnabled()) {
            final Pattern pattern = Pattern.compile("%[A-Za-z_]+%", Pattern.MULTILINE);
            final Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                for (int j = 0; j <= matcher.groupCount(); j++) {
                    String val = PlaceholderAPI.setPlaceholders(Bukkit.getPlayer(player), matcher.group(j));
                    line = line.replace(matcher.group(j), val);
                }
            }
        }
        return line;
    }

    private void editTitle(Objective objective, String text) {
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(text);
    }
}
