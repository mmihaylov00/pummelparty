package com.divictusgaming.PummelParty.util;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class VersionChecker {
    private static final String URL_ADDRESS = "https://api.spigotmc.org/legacy/update.php?resource=%%__RESOURCE__%%";
    public static final String version = "x.x.x";

    public static boolean isLatestVersion() throws IOException {
        URL requestUrl = new URL(URL_ADDRESS);
        HttpURLConnection connection = (HttpURLConnection) requestUrl.openConnection();
        connection.setRequestMethod("GET");
        connection.setDoOutput(true);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder latestVersion = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            latestVersion.append(inputLine);
        }

        in.close();
        connection.disconnect();

        return version.equals(latestVersion.toString());
    }
}
