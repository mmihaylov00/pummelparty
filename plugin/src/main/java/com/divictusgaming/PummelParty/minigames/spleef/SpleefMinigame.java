package com.divictusgaming.PummelParty.minigames.spleef;

import com.divictusgaming.PummelParty.PummelPartyMain;
import com.divictusgaming.PummelParty.util.UMaterials;
import com.divictusgaming.PummelParty.board.Board;
import com.divictusgaming.PummelParty.minigames.Minigame;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.UUID;

public class SpleefMinigame extends Minigame implements Listener {

    public SpleefMinigame(List<Location> spawnPoints, Board board, Location spectatorSpawn, int timer, int goal,
                          int gameWaitTime, List<ItemStack> items, boolean noFood) {
        super(spawnPoints, board, spectatorSpawn, timer, goal, gameWaitTime, items, noFood);
    }

    @Override
    public Listener enableEventHandlers() {
        Bukkit.getServer().getPluginManager().registerEvents(this, PummelPartyMain.getPlugin());
        return this;
    }

    @Override
    public void sendGameExplanationMessage() {
        for (UUID p : this.getBoard().getPlayers()) {
            //todo make configurable
            Bukkit.getPlayer(p).sendMessage("Welcome to the spleef minigame");
            Bukkit.getPlayer(p).sendMessage("Break the blocks below the player and send them to the void!");
        }
    }

    //GAME EVENTS BELOW

    @EventHandler
    public void onBlockBreak(BlockDamageEvent event) {
        for (UUID player : this.getBoard().getPlayers()) {
            if (player.equals(event.getPlayer().getUniqueId())) {
                event.setInstaBreak(true);
                ItemStack snowball = UMaterials.SNOWBALL.getItemStack();
                snowball.setAmount(2);
                event.getPlayer().getInventory().addItem(snowball);
                return;
            }
        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        //todo send message
        for (UUID player : this.getBoard().getPlayers()) {
            if (player.equals(event.getEntity().getUniqueId())) {
                addToDeque(event.getEntity().getUniqueId());
                setSpectator(event.getEntity());
                return;
            }
        }
    }

    @EventHandler
    //we don't want the player to die when getting hit by another player
    public void onDamage(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            for (UUID player : this.getBoard().getPlayers()) {
                if (event.getEntity().getUniqueId() == player) {
                    ((Player) event.getEntity()).setHealth(20);
                    break;
                }
            }
        }
    }
}
