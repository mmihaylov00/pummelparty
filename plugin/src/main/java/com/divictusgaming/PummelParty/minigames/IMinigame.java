package com.divictusgaming.PummelParty.minigames;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.UUID;

public interface IMinigame {
    void loadGame();

    Listener enableEventHandlers();

    void endGame();

    void resetArena();

    void sendGameExplanationMessage();

    void teleportPlayers();

    void setSpectator(Player player);

    void runGame();

    void waitTime();

    void addToDeque(UUID player);
}
