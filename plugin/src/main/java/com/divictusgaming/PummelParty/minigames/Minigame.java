package com.divictusgaming.PummelParty.minigames;

import com.divictusgaming.PummelParty.PummelPartyMain;
import com.divictusgaming.PummelParty.board.Board;
import com.divictusgaming.PummelParty.characters.PlayerCharacter;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayDeque;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class Minigame implements IMinigame, Listener {
    private final List<Location> spawnPoints;
    private final Board board;
    private final Location spectatorSpawn;
    private int timer;
    private int goal = -1;
    private int gameWaitTime;
    private final ArrayDeque<UUID> queue = new ArrayDeque<>();
    private Listener listener;
    private final List<ItemStack> items;
    private final boolean noFood;

    public Minigame(List<Location> spawnPoints, Board board, Location spectatorSpawn, int timer, int goal,
                    int gameWaitTime, List<ItemStack> items, boolean noFood) {
        this.spawnPoints = spawnPoints;
        this.board = board;
        this.spectatorSpawn = spectatorSpawn;
        this.timer = timer;
        this.goal = goal;
        this.gameWaitTime = gameWaitTime;
        this.items = items;
        this.noFood = noFood;
    }

    //todo check if a player is outside of the border, if yes, kill him,  if he is spectator, teleport him to spectator place
    //todo implement other games ONLY when this is fully working!

    @Override
    public void loadGame() {
        teleportPlayers();
        sendGameExplanationMessage();
        if (noFood) Bukkit.getServer().getPluginManager().registerEvents(this, PummelPartyMain.getPlugin());
        //loop all players
        for (UUID uuid : board.getPlayers()) {
            Player player = Bukkit.getPlayer(uuid);
            if (player == null) continue;
            //clear inventory and give items
            player.getInventory().clear();
            for (ItemStack item : items) {
                player.getInventory().addItem(item);
            }
            //set his hand on the first slot
            player.getInventory().setHeldItemSlot(0);
            player.setHealth(20);
            player.setFoodLevel(20);
        }
        waitTime();
    }

    @Override
    public void waitTime() {
        new BukkitRunnable() {
            @Override
            public void run() {
                //start the game if the timer is on 0
                if (gameWaitTime-- == 0) {
                    this.cancel();
                    teleportPlayers();
                    //sets the listener so it can be easier disabled at the end
                    listener = enableEventHandlers();
                    runGame();
                }
            }
        }.runTaskTimer(PummelPartyMain.getPlugin(), 0, 20);
    }

    @Override
    public void runGame() {
        new BukkitRunnable() {
            //update stuff every second
            @Override
            public void run() {
                //start the game if the timer is on 0
                if (timer == 0) {
                    this.cancel();
                    endGame();
                } else {
                    for (PlayerCharacter character : board.getPlayersCharacters()) {
                        if (character.getMinigamePoints() == goal) {
                            this.cancel();
                            endGame();
                        }
                    }
                    if (queue.size() == board.getPlayersMap().size() - 1) {
                        this.cancel();
                        endGame();
                    }
                }
                timer--;
            }
        }.runTaskTimer(PummelPartyMain.getPlugin(), 0, 20);
    }

    @Override
    public void endGame() {
        //stop the listeners
        HandlerList.unregisterAll(listener);
        if (noFood) HandlerList.unregisterAll(this);
        finishQueue();
        //teleport back to the board

        //todo return everyone to board and continue board game
        resetArena();
    }

    @Override
    public void resetArena() {
        //todo run arena blocks reset
    }

    @Override
    public void teleportPlayers() {
        UUID[] players = board.getPlayers();
        for (int i = 0; i < players.length; i++) {
            Player player = Bukkit.getPlayer(players[i]);
            if (player != null) player.teleport(spawnPoints.get(i));
        }
    }

    @Override
    public void setSpectator(Player player) {
        player.spigot().respawn();
        player.teleport(spectatorSpawn);
        player.setGameMode(GameMode.SPECTATOR);
    }

    @Override
    public void addToDeque(UUID player) {
        queue.addFirst(player);
    }

    private void finishQueue() {
        //get the players ordered from last to first
        List<PlayerCharacter> players = this.board.getPlayersCharacters().stream().sorted((c1, c2) -> {
            if (c1.getMinigamePoints() == c2.getMinigamePoints()) {
                if (c1.getGameTrophies() == c2.getGameTrophies())
                    return c2.getGameKeys() - c1.getGameKeys();
                return c2.getGameTrophies() - c1.getGameTrophies();
            }
            return c1.getMinigamePoints() - c2.getMinigamePoints();
        }).collect(Collectors.toList());

        for (PlayerCharacter player : players) {
            //if the player is already in the queue, no need to add him
            if (!queue.contains(player.getPlayerUUID()))
                queue.addFirst(player.getPlayerUUID());
        }

        //board should already be empty, but just to make sure
        board.getQueue().clear();
        //add the players to the queue
        queue.forEach(uuid -> board.getQueue().add(board.getPlayersMap().get(uuid)));
    }

    protected Board getBoard() {
        return board;
    }

    @EventHandler
    //we don't want the players to lose food level, most of the time
    public void feed(FoodLevelChangeEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        for (UUID uuid : this.board.getPlayers()) {
            if (uuid.equals(event.getEntity().getUniqueId())) {
                ((Player) event.getEntity()).setFoodLevel(20);
                break;
            }
        }
    }
}
