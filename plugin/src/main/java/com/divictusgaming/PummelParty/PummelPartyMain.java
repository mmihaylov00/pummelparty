package com.divictusgaming.PummelParty;

import com.divictusgaming.PummelParty.board.Board;
import com.divictusgaming.PummelParty.commands.MainCommand;
import com.divictusgaming.PummelParty.data.yaml.YAMLWorker;
import com.divictusgaming.PummelParty.data.yaml.dto.ConfigYAMLConfiguration;
import com.divictusgaming.PummelParty.data.yaml.dto.MessagesYAMLConfiguration;
import com.divictusgaming.PummelParty.database.FileDatabase;
import com.divictusgaming.PummelParty.database.IDatabase;
import com.divictusgaming.PummelParty.database.SQLDatabase;
import com.divictusgaming.PummelParty.enums.ScoreboardType;
import com.divictusgaming.PummelParty.events.EventManager;
import com.divictusgaming.PummelParty.papi.PAPI;
import com.divictusgaming.PummelParty.storage.BoardStorage;
import com.divictusgaming.PummelParty.storage.CharacterStorage;
import com.divictusgaming.PummelParty.util.ScoreboardManager;
import com.divictusgaming.PummelParty.util.StringFormater;
import com.divictusgaming.PummelParty.wizards.BoardWizard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;

public class PummelPartyMain extends JavaPlugin {
    private static com.divictusgaming.PummelParty.nms.INMSMain INMSMain;
    private static PummelPartyMain plugin;
    private static ScoreboardManager scoreboards;
    private static boolean papiEnabled = false;
    private static File dataFolder;
    private static BoardWizard boardWizzard;
    private static MessagesYAMLConfiguration messages;
    private static ConfigYAMLConfiguration config;
    private static IDatabase database;

    private static CharacterStorage characterStorage;
    private static BoardStorage boardStorage;


    //key is the board's name
    @Override
    public void onEnable() {
        dataFolder = getDataFolder().getAbsoluteFile();
        if (!setupVersion()) {
            getLogger().info("Incompatible version, disabling!");
            return;
        }
        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            new PAPI().register();
            papiEnabled = true;
        }
        plugin = this;
        scoreboards = new ScoreboardManager();
        //register all events in a single class(My experience says its less laggy this way (:)
        Bukkit.getPluginManager().registerEvents(new EventManager(), plugin);
        MainCommand mainCommand = new MainCommand();
        getCommand("pummelparty").setExecutor(mainCommand);
        getCommand("pummelparty").setTabCompleter(mainCommand);
        getLogger().info("PummelParty Enabled");
        //load all boards when plugin load
        reload();

        if (database instanceof SQLDatabase && !((SQLDatabase) database).isConnected()) {
            Bukkit.getConsoleSender().sendMessage(messages.getPrefix() + ChatColor.RED + "Could not connect to the Database!");
            return;
        }
    }

    public static void reload() {
        boardWizzard = new BoardWizard();
        boardStorage = new BoardStorage();

        //create a new messages file
        messages = new MessagesYAMLConfiguration();
        if (new File(dataFolder + "/messages.yml").exists()) {
            //if the file exists, load it
            YAMLWorker.readFile(messages);
            //we add all missing messages (added from newer version)
            YAMLWorker.addMissing(messages);
        } else {
            //if the file doesn't exist, create it with the default values from the object
            YAMLWorker.saveFile(messages);
        }

        //create a new cofnig file
        config = new ConfigYAMLConfiguration();
        if (new File(dataFolder + "/config.yml").exists()) {
            //if the file exists, load it
            YAMLWorker.readFile(config);
            //we add all missing config values (added from newer version)
            YAMLWorker.addMissing(config);
        } else {
            //if the file doesn't exist, create it with the default values from the object
            YAMLWorker.saveFile(config);
        }
        //format all messages to contain the colors, load once, no need to replace them every time we send the message
        StringFormater.formatAllFields(messages);
        StringFormater.formatAllFields(config);

        if (characterStorage != null) characterStorage.saveAll();
        connectToDb();
        characterStorage = new CharacterStorage();

        scoreboards.reloadScoreboards();
        for (Player player : Bukkit.getOnlinePlayers()) {
            scoreboards.setScoreboard(player.getUniqueId(), ScoreboardType.LOBBY);
            characterStorage.getCharacter(player).leaveBoard();
            if (config.getSpawnLocation() != null) {
                player.teleport(config.getSpawnLocation());
            }
        }

        for (Player player : Bukkit.getOnlinePlayers()) {
            for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
                if (player.getUniqueId() != onlinePlayer.getUniqueId()) {
                    INMSMain.showPlayer(player, onlinePlayer, plugin);
                }
            }
        }
    }

    private static void connectToDb() {

        if (database != null)
            if (database instanceof SQLDatabase) ((SQLDatabase) database).closeConnection();

        switch (config.getDatabaseType().toLowerCase()) {
            case "mysql":
                database = new SQLDatabase();
                break;
            case "file":
            default:
                database = new FileDatabase();
        }

    }

    private boolean setupVersion() {
        String version;
        try {
            version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        } catch (ArrayIndexOutOfBoundsException whatVersionAreYouUsingException) {
            return false;
        }
        switch (version) {
            case "v1_12_R1":
                INMSMain = new com.divictusgaming.PummelParty.nms.v1_12_R1.NMSMain();
                break;
            case "v1_13_R1":
                INMSMain = new com.divictusgaming.PummelParty.nms.v1_13_R1.NMSMain();
                break;
            case "v1_13_R2":
                INMSMain = new com.divictusgaming.PummelParty.nms.v1_13_R2.NMSMain();
                break;
            case "v1_14_R1":
                INMSMain = new com.divictusgaming.PummelParty.nms.v1_14_R1.NMSMain();
                break;
            case "v1_15_R1":
                INMSMain = new com.divictusgaming.PummelParty.nms.v1_15_R1.NMSMain();
                break;
            case "v1_16_R1":
                INMSMain = new com.divictusgaming.PummelParty.nms.v1_16_R1.NMSMain();
                break;
            case "v1_16_R2":
                INMSMain = new com.divictusgaming.PummelParty.nms.v1_16_R2.NMSMain();
                break;
            case "v1_16_R3":
                INMSMain = new com.divictusgaming.PummelParty.nms.v1_16_R3.NMSMain();
                break;
        }
        return INMSMain != null;
    }

    @Override
    public void onDisable() {
        //remove metatag from all players (from gadgets)
        for (Player player : Bukkit.getOnlinePlayers())
            if (player.hasMetadata("PummelParty.GadgetUsing"))
                player.removeMetadata("PummelParty.GadgetUsing", this);
        //save all player's data
        characterStorage.saveAll();
        if (database instanceof SQLDatabase)
            ((SQLDatabase) database).closeConnection();

        getLogger().info("PummelParty Disabled");
    }

    public static com.divictusgaming.PummelParty.nms.INMSMain getINMSMain() {
        return INMSMain;
    }

    public static PummelPartyMain getPlugin() {
        return plugin;
    }

    public static File getPluginsFolder() {
        return dataFolder;
    }

    public static HashMap<String, Board> getBoards() {
        return boardStorage.getBoards();
    }

    public static BoardWizard getBoardWizzard() {
        return boardWizzard;
    }

    public static MessagesYAMLConfiguration getMessages() {
        return messages;
    }

    public static ScoreboardManager getScoreboardManager() {
        return scoreboards;
    }

    public static CharacterStorage getCharacterStorage() {
        return characterStorage;
    }

    public static ConfigYAMLConfiguration getConfiguration() {
        return config;
    }

    public static boolean isPapiEnabled() {
        return papiEnabled;
    }

    public static IDatabase getDatabase() {
        return database;
    }
}
