package com.divictusgaming.PummelParty.events;

import com.divictusgaming.PummelParty.PummelPartyMain;
import com.divictusgaming.PummelParty.board.Board;
import com.divictusgaming.PummelParty.enums.ScoreboardType;
import com.divictusgaming.PummelParty.gadgets.GadgetsEnum;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class EventManager implements Listener {
    //add every event we use here & push it to all classes that use it.
    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        PummelPartyMain.getBoardWizzard().blockSelectHandle(event);
        //test all gadgets
        for (GadgetsEnum gadget : GadgetsEnum.values())
            gadget.getGadget().execute(event);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        PummelPartyMain.getBoardWizzard().guiActionHandle(event);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Location spawn = PummelPartyMain.getConfiguration().getSpawnLocation();
        if (spawn != null) {
            event.getPlayer().teleport(spawn);
        }
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            if (onlinePlayer.getUniqueId() != event.getPlayer().getUniqueId()) {
                if (PummelPartyMain.getCharacterStorage().getCharacter(onlinePlayer).getBoard() != null) {
                    event.getPlayer().hidePlayer(PummelPartyMain.getPlugin(), onlinePlayer);
                    onlinePlayer.hidePlayer(PummelPartyMain.getPlugin(), event.getPlayer());
                }
            }
        }
        PummelPartyMain.getCharacterStorage().getCharacter(event.getPlayer());//this will load player's data when he logs in
        PummelPartyMain.getScoreboardManager().setScoreboard(event.getPlayer().getUniqueId(), ScoreboardType.LOBBY);
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        //leave a joined board on quit
        Board board = PummelPartyMain.getCharacterStorage().getCharacter(event.getPlayer()).getBoard();
        if (board != null) board.leave(event.getPlayer());

        PummelPartyMain.getCharacterStorage().unloadCharacter(event.getPlayer());    //this will unload data when a player leaves
        PummelPartyMain.getScoreboardManager().removeScoreboard(event.getPlayer().getUniqueId());
    }
}
