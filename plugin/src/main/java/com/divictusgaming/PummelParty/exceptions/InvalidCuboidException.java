package com.divictusgaming.PummelParty.exceptions;

public class InvalidCuboidException extends IllegalArgumentException {

    public InvalidCuboidException(String message) {
        super(message);
    }

}
