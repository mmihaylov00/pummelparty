package com.divictusgaming.PummelParty.database;


import com.divictusgaming.PummelParty.PummelPartyMain;
import com.divictusgaming.PummelParty.characters.PlayerCharacter;
import com.divictusgaming.PummelParty.data.PlayerConfiguration;
import com.divictusgaming.PummelParty.data.mysql.PlayerMySQLConfiguration;
import com.divictusgaming.PummelParty.data.mysql.annotations.MySQLColumn;
import com.divictusgaming.PummelParty.data.yaml.dto.ConfigYAMLConfiguration;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.UUID;

public class SQLDatabase implements IDatabase {
    private Connection connection;
    private final String TABLE_NAME = "players";
    private Boolean isConnected = true;

    public SQLDatabase() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }
        ConfigYAMLConfiguration config = PummelPartyMain.getConfiguration();
        connect(config.getMysqlUrl(), config.getMysqlUsername(), config.getMysqlPassword());
    }

    private void connect(String url, String username, String password) {
        try {
            connection = DriverManager.getConnection(url, username, password);
            generateTable();
        } catch (SQLException e) {
            if (!url.endsWith("&characterEncoding=latin1")) {
                connect(url + "&characterEncoding=latin1", username, password);
            } else {
                isConnected = false;
                e.printStackTrace();
            }
        }
    }


    public void closeConnection() {
        try {
            if (connection != null && !connection.isClosed())
                connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void generateTable() {
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(uuid varchar(64) PRIMARY KEY NOT NULL);";
        try {
            connection.prepareStatement(sql).executeUpdate();
            generateColumns();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void generateColumns() {
        for (Field declaredField : PlayerMySQLConfiguration.class.getDeclaredFields()) {
            if (declaredField.isAnnotationPresent(MySQLColumn.class)) {
                declaredField.setAccessible(true);
                String sql = "ALTER TABLE " + TABLE_NAME + " ADD " + declaredField.getName() + " " +
                        declaredField.getAnnotation(MySQLColumn.class).value() + " DEFAULT 0 NOT NULL;";
                try {
                    connection.prepareStatement(sql).executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                declaredField.setAccessible(false);
            }
        }
    }

    @Override
    public PlayerConfiguration loadData(UUID uuid) {
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE uuid = ?;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid.toString());
            ResultSet result = preparedStatement.executeQuery();

            if (result.next()) new PlayerMySQLConfiguration(result);
            else createData(uuid);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new PlayerMySQLConfiguration();
    }

    @Override
    public void saveData(PlayerCharacter player) {
        if (player.isUpdated()) {
            StringBuilder sb = new StringBuilder().append("UPDATE " + TABLE_NAME + " SET ");
            PlayerMySQLConfiguration playerData = new PlayerMySQLConfiguration(player);

            for (Field declaredField : playerData.getClass().getDeclaredFields()) {
                if (declaredField.isAnnotationPresent(MySQLColumn.class)) {
                    declaredField.setAccessible(true);
                    sb.append(declaredField.getName()).append(" = ");
                    try {
                        switch (declaredField.getAnnotation(MySQLColumn.class).value()) {
                            case "INT":
                                sb.append(declaredField.getInt(playerData));
                                break;
                            case "VARCHAR":
                            default:
                                sb.append("'").append(declaredField.get(playerData)).append("'");
                        }
                    }catch (IllegalAccessException | IllegalArgumentException e){
                        e.printStackTrace();
                    }
                    sb.append(", ");
                    declaredField.setAccessible(false);
                }
            }
            sb.delete(sb.length() - 2, sb.length());
            sb.append(" WHERE uuid = ?;");

            try {
                PreparedStatement preparedStatement = connection.prepareStatement(sb.toString());
                preparedStatement.setString(1, player.getPlayerUUID().toString());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    private void createData(UUID uuid) {
        String sql = "INSERT INTO " + TABLE_NAME + " (uuid) values(?);";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uuid.toString());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Boolean isConnected() {
        return isConnected;
    }
}
