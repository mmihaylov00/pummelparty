package com.divictusgaming.PummelParty.database;


import com.divictusgaming.PummelParty.characters.PlayerCharacter;
import com.divictusgaming.PummelParty.data.PlayerConfiguration;

import java.util.UUID;

public interface IDatabase {
    PlayerConfiguration loadData(UUID uuid);
    void saveData(PlayerCharacter player);
}
