package com.divictusgaming.PummelParty.database;

import com.divictusgaming.PummelParty.characters.PlayerCharacter;
import com.divictusgaming.PummelParty.data.PlayerConfiguration;
import com.divictusgaming.PummelParty.data.yaml.YAMLWorker;
import com.divictusgaming.PummelParty.data.yaml.dto.PlayerYAMLConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class FileDatabase implements IDatabase {

    @Override
    public PlayerConfiguration loadData(UUID uuid) {
        Map<String, String> placeholder = new HashMap<>();
        placeholder.put("uuid", uuid.toString());
        PlayerYAMLConfiguration config = new PlayerYAMLConfiguration();
        YAMLWorker.readFile(config, placeholder);
        return config;
    }

    @Override
    public void saveData(PlayerCharacter player) {
        Map<String, String> placeholder = new HashMap<>();
        placeholder.put("uuid", player.getPlayerUUID().toString());
        YAMLWorker.saveFile(new PlayerYAMLConfiguration(player), placeholder);
    }
}
