package com.divictusgaming.PummelParty.nms.v1_14_R1;

import com.divictusgaming.PummelParty.nms.INMSMain;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class NMSMain implements INMSMain {
    @Override
    public void hidePlayer(Player p, Player p2, Plugin plugin) {
        p.hidePlayer(plugin, p2);
    }

    @Override
    public void showPlayer(Player p, Player p2, Plugin plugin) {
        p.showPlayer(plugin, p2);
    }
}