package com.divictusgaming.PummelParty.nms;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public interface INMSMain {
    public void hidePlayer(Player p, Player p2, Plugin plugin);
    public void showPlayer(Player p, Player p2, Plugin plugin);
}
